﻿using System;
using System.Diagnostics;

namespace CellAO.Studio
{
    public class ProcessItem
    {
        public String Display { get; set; }
        public Process Value { get; set; }

        public override String ToString()
        {
            return Value.ProcessName + " - " + Value.Id;
        }
    }
}
