﻿#region License

// Copyright (c) 2005-2014, CellAO Team
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#endregion

namespace CellAO.Studio
{
    #region Usings ...

    using System;
    using System.Runtime.Remoting;
    using System.Runtime.Remoting.Channels.Ipc;

    using EasyHook;
    using AOInject;

    #endregion

    /// <summary>
    /// </summary>
    public class AOHook
    {
        #region Static Fields

        /// <summary>
        /// </summary>
        public static IpcServerChannel IpcChannel = null;

        /// <summary>
        /// </summary>
        private static string channelName = null;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// </summary>
        /// <param name="processId">
        /// </param>
        /// <returns>
        /// </returns>
        public static bool Inject(int processId, string channelName = null)
        {
            try
            {
                if (IpcChannel == null)
                {
                    IpcChannel = RemoteHooking.IpcCreateServer<HookInterface>(
                        ref channelName,
                        WellKnownObjectMode.Singleton);
                }
                else
                {
                    IpcChannel.StopListening(null);
                    IpcChannel = null;
                    IpcChannel = RemoteHooking.IpcCreateServer<HookInterface>(
                        ref channelName,
                        WellKnownObjectMode.Singleton);
                }

                RemoteHooking.Inject(processId, "AOInject.dll", "AOInject.dll", channelName);

                return true;
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.ToString());
                return false;
            }
        }

        #endregion
    }
}