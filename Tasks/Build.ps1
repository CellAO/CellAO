Param(
    [string]$BaseDir
)
  
$outputFolder = $BaseDir + "Output"

$msbuild = """${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe"""
$options = "/p:Configuration=Debug /p:Platform=x64"
$releaseFolder = $BaseDir + "CellAO\Built\Debug"

if ([System.IO.Directory]::Exists($outputFolder))
{
 [System.IO.Directory]::Delete($outputFolder, 1)
}

# make sure our working directory is correct
Set-Location $BaseDir

# create the build command and invoke it 
# note that if you want to debug, remove the "/noconsolelogger" 
# from the $options string
$clean = $msbuild + " CellAO\CellAO.sln " + $options + " /t:Clean"
$build = $msbuild + " CellAO\CellAO.sln " + $options + " /t:Build"

cmd /c $build

# move all the files that were built to the output folder
xcopy $releaseFolder $outputFolder /s /i /q