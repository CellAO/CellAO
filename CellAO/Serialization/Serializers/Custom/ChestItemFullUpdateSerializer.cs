﻿
namespace CellAO.Messages.Messages.Serialization.Serializers.Custom
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Text;

    using CellAO.Enums;
    using CellAO.Messages.N3Messages;
    using CellAO.Interfaces;
    using CellAO.Messages.Fields;

    public class ChestItemFullUpdateSerializer : ISerializer
    {
        public ChestItemFullUpdateSerializer()
        {
            this.Type = typeof(ChestItemFullUpdateSerializer);
        }

        public Type Type { get; }

        #region Public Methods and Operators

        public object Deserialize(StreamReader streamReader, SerializationContext serializationContext, PropertyMetaData propertyMetaData = null)
        {
            var chestMsg = new ChestItemFullUpdateMessage();
            chestMsg.N3MessageType = (N3MessageType)streamReader.ReadInt32();
            chestMsg.Identity = streamReader.ReadIdentity();
            chestMsg.Unknown = streamReader.ReadByte();

            chestMsg.SifuMsgVersion = streamReader.ReadInt32();
            chestMsg.Owner = streamReader.ReadIdentity();
            chestMsg.Playfield = streamReader.ReadInt32();
            chestMsg.Unknown1 = streamReader.ReadIdentity();
            chestMsg.Unknown2 = streamReader.ReadByte();
            chestMsg.Unknown3 = streamReader.ReadByte();

            var statCount = streamReader.ReadInt32() / 1009 - 1;
            chestMsg.Stats = new GameTuple<StatIds, uint>[statCount];

            for (int i = 0; i < statCount; i++) {
                var value1 = (StatIds)streamReader.ReadInt32();
                var value2 = streamReader.ReadUInt32();
                chestMsg.Stats[i] = new GameTuple<StatIds, uint>() { Value1 = value1, Value2 = value2 };
            }

            var nameLength = streamReader.ReadInt32();
            chestMsg.Name = streamReader.ReadString(nameLength);

            chestMsg.LifuMsgVersion = streamReader.ReadInt32();
            chestMsg.Unknown4 = streamReader.ReadInt32();
            chestMsg.Unknown5 = streamReader.ReadInt32();
            chestMsg.CifuMsgVersion= streamReader.ReadInt32();

            return chestMsg;
        }

        public Expression DeserializerExpression(ParameterExpression streamReaderExpression, ParameterExpression serializationContextExpression, Expression assignmentTargetExpression, PropertyMetaData propertyMetaData)
        {

            var deserializerMethodInfo =
                ReflectionHelper
                    .GetMethodInfo
                    <ChestItemFullUpdateSerializer, Func<StreamReader, SerializationContext, PropertyMetaData, object>>
                    (o => o.Deserialize);
            var serializerExp = Expression.New(this.GetType());
            var callExp = Expression.Call(
                serializerExp,
                deserializerMethodInfo,
                new Expression[]
                    {
                        streamReaderExpression, serializationContextExpression,
                        Expression.Constant(propertyMetaData, typeof(PropertyMetaData))
                    });

            var assignmentExp = Expression.Assign(
                assignmentTargetExpression, Expression.TypeAs(callExp, assignmentTargetExpression.Type));
            return assignmentExp;
        }

        public void Serialize(
            StreamWriter streamWriter,
            SerializationContext serializationContext,
            object value,
            PropertyMetaData propertyMetaData = null)
        {
            if (!(value is ChestItemFullUpdateMessage)) return;

            var chestMsg = (ChestItemFullUpdateMessage)value;

            streamWriter.WriteInt32((int)chestMsg.N3MessageType);
            streamWriter.WriteIdentity(chestMsg.Identity);
            streamWriter.WriteByte(0x00);//chestMsg.Unknown);
            streamWriter.WriteInt32(chestMsg.SifuMsgVersion);
            streamWriter.WriteIdentity(chestMsg.Owner);
            streamWriter.WriteInt32(chestMsg.Playfield);
            streamWriter.WriteIdentity(chestMsg.Unknown1);
            streamWriter.WriteByte(chestMsg.Unknown2);
            streamWriter.WriteByte(chestMsg.Unknown3);

            streamWriter.WriteInt32((chestMsg.Stats.Length + 1) * 1009);

            foreach (var stat in chestMsg.Stats)
            {
                streamWriter.WriteInt32((int)stat.Value1);
                streamWriter.WriteUInt32(stat.Value2);
            }

            streamWriter.WriteInt32(chestMsg.Name.Length);
            streamWriter.WriteString(chestMsg.Name);

            streamWriter.WriteInt32(chestMsg.LifuMsgVersion);
            streamWriter.WriteInt32(chestMsg.Unknown4);
            streamWriter.WriteInt32(chestMsg.Unknown5);
            streamWriter.WriteInt32(chestMsg.CifuMsgVersion);
        }

        public Expression SerializerExpression(
            ParameterExpression streamWriterExpression,
            ParameterExpression serializationContextExpression,
            Expression valueExpression,
            PropertyMetaData propertyMetaData)
        {
            var serializerMethodInfo =
                ReflectionHelper
                    .GetMethodInfo
                    <ChestItemFullUpdateSerializer,
                        Action<StreamWriter, SerializationContext, object, PropertyMetaData>>(o => o.Serialize);
            var serializerExp = Expression.New(this.GetType());
            var callExp = Expression.Call(
                serializerExp,
                serializerMethodInfo,
                new[]
                    {
                        streamWriterExpression, serializationContextExpression, valueExpression,
                        Expression.Constant(propertyMetaData, typeof(PropertyMetaData))
                    });
            return callExp;
        }

        #endregion
    }
}