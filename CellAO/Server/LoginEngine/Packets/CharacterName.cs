﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace LoginEngine.Packets
{
    #region Usings ...

    using System;
    using System.Collections.Generic;
    using System.Text;

    using CellAO.Database.Dao;
    using CellAO.Database.Entities;

    using CellAO.Enums;
    using CellAO.Messages.Fields;

    #endregion

    /// <summary>
    /// </summary>
    public class CharacterName
    {
        #region Static Fields

        /// <summary>
        /// </summary>
        private static string mandatoryVowel = "aiueo"; /* 5 chars */

        /// <summary>
        /// </summary>
        private static string optionalOrdCon = "vybcfghjqktdnpmrlws"; /* 19 chars */

        /// <summary>
        /// </summary>
        private static string optionalOrdEnd = "nmrlstyzx"; /* 9 chars */

        #endregion

        #region Public Properties

        /// <summary>
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// </summary>
        public int Breed { get; set; }

        /// <summary>
        /// </summary>
        public int Fatness { get; set; }

        /// <summary>
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// </summary>
        public int HeadMesh { get; set; }

        /// <summary>
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// </summary>
        public int MonsterScale { get; set; }

        /// <summary>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// </summary>
        public int Profession { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// </summary>
        private int[] Abis { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public int CheckAgainstDatabase()
        {
            /* name in use */
            if (CharacterDao.Instance.ExistsByName(this.Name))
            {
                return 0;
            }

            return this.CreateNewChar();
        }

        /// <summary>
        /// </summary>
        /// <param name="charid">
        /// </param>
        public void DeleteChar(int charid)
        {
            try
            {
                CharacterDao.Instance.Delete(charid);
            }
            catch (Exception e)
            {
                Console.WriteLine(this.Name + e.Message);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="profession">
        /// </param>
        /// <returns>
        /// </returns>
        public string GetRandomName(Profession profession)
        {
            var random = new Random();
            byte randomNameLength = 0;
            var randomLength = (byte)random.Next(3, 8);
            var sb = new StringBuilder();
            while (randomNameLength <= randomLength)
            {
                if (random.Next(14) > 4)
                {
                    sb.Append(optionalOrdCon.Substring(random.Next(0, 18), 1));
                    randomNameLength++;
                }

                sb.Append(mandatoryVowel.Substring(random.Next(0, 4), 1));
                randomNameLength++;

                if (random.Next(14) <= 4)
                {
                    continue;
                }

                sb.Append(optionalOrdEnd.Substring(random.Next(0, 8), 1));
                randomNameLength++;
            }

            string name = sb.ToString();
            name = char.ToUpper(name[0]) + name.Substring(1);
            return name;
        }

        /// <summary>
        /// </summary>
        /// <param name="startInSL">
        /// </param>
        /// <param name="charid">
        /// </param>
        public void SendNameToStartPlayfield(bool startInSL, int PFStart, int charid)
        {
            int playfield, x, y, z;

            if (startInSL) // Duh Shadowlands
            {
                playfield = 4001; 
                x = 850;
                y = 43;
                z = 565;
            }

            if (PFStart == 1) // Starter Island
            {
                playfield = 4582;
                x = 939;
                y = 20;
                z = 732;
            }
            else if (PFStart == 2) // New Player Experiance
            {
                playfield = 6553;
                x = 3609;
                z = 786;
                y = 53;
            }
            else if (PFStart == 3)
            {
                //TODO: Gather all BY info for Random BY Start
                playfield = 6553;
                x = 3609;
                z = 786;
                y = 53;
            }
            else if (PFStart == 4)
            {
                //TODO: Find the PF Data for the Starting Space Station to readd that!
                playfield = 6553;
                x = 3609;
                z = 786;
                y = 53;
            }
            else
            {
                //Default Starting if something goes wrong in the Config..
                playfield = 6553;
                x = 3609;
                z = 786;
                y = 53;
            }
            DBCharacter character = CharacterDao.Instance.Get(charid);
            if (character != null)
            {
                CharacterDao.Instance.Save(
                    character // woo....
                    ,
                    new { Id = charid, Playfield = playfield, X = x, Y = y, Z = z });

                CharacterDao.Instance.SetPlayfield(charid, (int)IdentityType.Playfield, playfield);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// </summary>
        /// <returns>
        /// charID
        /// </returns>
        private int CreateNewChar()
        {
            const int DEFAULT_TYPE = 50000;

            DBCharacter newCharacter = new DBCharacter
                                       {
                                           FirstName = string.Empty,
                                           LastName = string.Empty,
                                           Name = this.Name,
                                           Username = this.AccountName,
                                       };

            CharacterDao.Instance.Add(newCharacter);

            int charID = newCharacter.Id;

            #region Statistics

            // default values depending on breeds 
            // TODO: move to database ?
            int strength, psychic, sense, intelligence, stamina, agility;

            switch ((Breed)this.Breed)
            {
                case CellAO.Enums.Breed.Solitus:
                    strength = 6;
                    psychic = 6;
                    sense = 6;
                    intelligence = 6;
                    stamina = 6;
                    agility = 6;
                    break;

                case CellAO.Enums.Breed.Opifex:
                    strength = 3;
                    psychic = 3;
                    sense = 10;
                    intelligence = 6;
                    stamina = 6;
                    agility = 15;
                    break;

                case CellAO.Enums.Breed.Nanomage:
                    strength = 3;
                    psychic = 10;
                    sense = 6;
                    intelligence = 15;
                    stamina = 3;
                    agility = 3;
                    break;

                case CellAO.Enums.Breed.Atrox:
                    strength = 15;
                    psychic = 3;
                    sense = 3;
                    intelligence = 3;
                    stamina = 10;
                    agility = 6;
                    break;

                default:
                    
                    Console.WriteLine("unknown breed: {0}", this.Breed);  // so we can debug

                    throw new NotImplementedException($"unknown breed: {this.Breed}");
            }

            List<DBStats> stats = new List<DBStats>();

            // Transmit GM level into stats table
            stats.Add(
                new DBStats
                {
                    Type = DEFAULT_TYPE,
                    Instance = charID,
                    StatId = (int) StatIds.gmlevel,
                    StatValue = LoginDataDao.Instance.GetByUsername(this.AccountName).GM // users' GM level
                });

            // Flags
            const int DEFAULT_FLAGS = 0x00081241; // 10000001001001000001
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.flags, StatValue = DEFAULT_FLAGS });

            // Level
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.level, StatValue = 1 });

            // SEXXX
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.sex, StatValue = this.Gender });

            // Headmesh
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.headmesh, StatValue = this.HeadMesh });

            // MonsterScale
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.monsterscale, StatValue = this.MonsterScale });

            // Visual Sex (even better ^^)
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.visualsex, StatValue = this.Gender });

            // Breed
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.breed, StatValue = this.Breed });

            // Visual Breed
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.visualbreed, StatValue = this.Breed });

            // Profession / 60
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.profession, StatValue = this.Profession });

            // VisualProfession / 368
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.visualprofession, StatValue = this.Profession });

            // Fatness / 47
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.fatness, StatValue = this.Fatness });

            // Strength / 16
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.strength, StatValue = strength });

            // Psychic / 21
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.psychic, StatValue = psychic });

            // Sense / 20
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.sense, StatValue = sense });

            // Intelligence / 19
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.intelligence, StatValue = intelligence });

            // Stamina / 18
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.stamina, StatValue = stamina });

            // Agility / 17
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.agility, StatValue = agility });

            // Set HP and NP to 1
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.life, StatValue = 1 });
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.currentnano, StatValue = 1 });

            // NPCFamily / 455
            stats.Add(new DBStats { Type = DEFAULT_TYPE, Instance = charID, StatId = (int) StatIds.npcfamily, StatValue = 0 });

            // Expansion Packs
            stats.Add(
                new DBStats
                {
                    Type = DEFAULT_TYPE,
                    Instance = charID,
                    StatId = (int) StatIds.expansion,
                    StatValue = LoginDataDao.Instance.GetByUsername(this.AccountName).Expansions
                });

            StatDao.Instance.BulkReplace(stats);

            #endregion

            return charID;
        }

        #endregion
    }
}