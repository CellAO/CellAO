﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace ZoneEngine.Core.MessageHandlers
{
    #region Usings ...

    using CellAO.Core.Components;
    using CellAO.Core.Entities;
    using CellAO.Core.Items;
    using CellAO.Core.Network;
    using CellAO.Database.Dao;
    using CellAO.Enums;

    using CellAO.Messages.Fields;
    using CellAO.Messages.N3Messages;

    using Utility;

    using ZoneEngine.Core.PacketHandlers;

    using System.Linq;

    #endregion

    /// <summary>
    /// </summary>
    [MessageHandler(MessageHandlerDirection.All)]
    public class AttackMessageHandler :
        BaseMessageHandler<AttackMessage, AttackMessageHandler>
    {
        /// <summary>
        /// </summary>
        public AttackMessageHandler()
        {

        }

        #region Inbound

        /// <summary>
        /// </summary>
        /// <param name="message">
        /// </param>
        /// <param name="client">
        /// </param>
        protected override void Read(AttackMessage message, IZoneClient client)
        {
            LogUtil.Debug(DebugInfoDetail.NetworkMessages, "Reading Attack Message");

            CellAO.Core.CharacterActionManager.Start();

            client.Controller.Character.SetFightingTarget(message.Target);


            var target = CellAO.ObjectManager.Pool.Instance.GetObject<Character>(message.Target);

            client.Controller.Character.BeginAutoAttack(target);

            client.Controller.Character.Send(new SpecialAttackWeaponMessage()
            {
                Identity = client.Controller.Character.Identity,
                Specials = (new System.Collections.Generic.List<SpecialAttack>()
                {
                  new SpecialAttack()
                  {
                      Unknown1 = 43712,//client.Controller.Character.GetRightHandWeapon().LowID,
                      Unknown2 = 144745,//client.Controller.Character.GetRightHandWeapon().HighID,
                      Unknown3 = 100,
                      Unknown4 = "MAAT"
                  },
                  new SpecialAttack()
                  {
                      Unknown1 = 42033,//client.Controller.Character.GetRightHandWeapon().LowID,
                      Unknown2 = 42032,//client.Controller.Character.GetRightHandWeapon().HighID,
                      Unknown3 = 144,
                      Unknown4 = "DIIT"
                  },
                  new SpecialAttack()
                  {
                      Unknown1 = 70292,//client.Controller.Character.GetRightHandWeapon().LowID,
                      Unknown2 = 70293,//client.Controller.Character.GetRightHandWeapon().HighID,
                      Unknown3 = 142,
                      Unknown4 = "BRAW"
                  },
                }).ToArray(),
                AggDef = client.Controller.Character.Stats[StatIds.aggdef].Value
            });
            
            client.Controller.Character.Send(new AttackMessage()
            {
                Identity = client.Controller.Character.Identity,
                Target = message.Target,
                Unknown = 0x00,
                Action = 0x00
            });
            

        }

        #endregion

        #region Outbound

        /// <summary>
        /// </summary>
        /// <param name="message">
        /// </param>
        /// <returns>
        /// </returns>
        private MessageDataFiller Reply(AttackMessage message)
        {
            return x =>
            {
            };
        }

        #endregion
    }
}