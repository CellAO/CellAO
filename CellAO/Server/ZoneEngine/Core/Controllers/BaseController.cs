﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace ZoneEngine.Core.Controllers
{
    #region Usings ...

    using System;
    using System.Linq;

    using CellAO.Core.Entities;
    using CellAO.Core.Functions;
    using CellAO.Core.Nanos;
    using CellAO.Enums;
    using CellAO.Interfaces;
    using CellAO.Messages.N3Messages;

    using ZoneEngine.Core.Functions;

    using Utility;
    using ZoneEngine.Core.MessageHandlers;
    #endregion

    public class BaseController
    {


        public virtual ICharacter Character
        {
            get
            {
                return this._character;
            }
            set
            {
                this._character = value;
            }
        }

        /// <summary>
        /// </summary>
        internal ICharacter _character { get; set; }

        public void ApplyGameFunction(ICharacter caster, ICharacter target, object nano, int funcIndex, Function func)
        {
            // If this function requires instant application... then you don't need to "apply" it
            if (func.InstantApplication)
            {
                target.Controller.CallFunction(func, caster);
                return;
            }

            ActiveNanoFunction activeNano = null;
            var maxNcu = target.Stats[StatIds.maxncu].Value;
            var currentNcu = target.Stats[StatIds.currentncu].Value;

            if (!((NanoFormula)nano).Flags.HasFlag(NanoFlags.NotRemovable) && ((NanoFormula)nano).NCUCost() + currentNcu > maxNcu)
            {
                target.Send(new FeedbackMessage()
                {
                    Identity = target.Identity,
                    Unknown = 1,
                    Unknown1 = 1107296280,
                    CategoryId = 110,
                    MessageId = 220179189 // Reject full ncu message
                });
                return;
            } else
            {
                // This could be a problem when applying functions that were not sourced from casting nanos... like items or functions that are nested inside parent functions
                Character.Playfield.Announce(new FeedbackMessage()
                {
                    Identity = Character.Identity,
                    Unknown = 1,
                    Unknown1 = 1107296280,
                    CategoryId = 110,
                    MessageId = 124550313  // Success message
                });
            }

            bool land = true;
            var existingStrain = target.ActiveNanos.Where(kv => ((NanoFormula)nano).NanoStrain().Equals(((CellAO.Core.Nanos.ActiveNanoFunction)kv.Value).Nano.NanoStrain()));
            if (existingStrain.Count() > 0)
            {
                activeNano = ((CellAO.Core.Nanos.ActiveNanoFunction)existingStrain.FirstOrDefault().Value);
                // ALREADY HAS NANO APPLIED FROM SAME CASTER & SAME STRAIN -> Refresh

                // If its the same nano you can just refresh it
                if (((CellAO.Core.Nanos.ActiveNanoFunction)existingStrain.FirstOrDefault().Value).Nano.ID == ((NanoFormula)nano).ID)
                {
                    LogUtil.Debug(DebugInfoDetail.GameFunctions, "Overriding nano with same strain");
                    existingStrain.FirstOrDefault().Value.LastTick = DateTime.UtcNow;
                    ((CellAO.Core.Nanos.ActiveNanoFunction)existingStrain.FirstOrDefault().Value).CurrentTickCount = 0;
                    land = false;
                }
                // If its a lesser ql nano you can cancel the lower and replace with higher
                else
                {
                    LogUtil.Debug(DebugInfoDetail.GameFunctions, "Overriding nano with higher ql strain");
                    ((PlayerController)((Character)target).Controller).CancelActiveNano(existingStrain.FirstOrDefault().Key, existingStrain.FirstOrDefault().Value.ID);
                }
            }

            if (land)
            {
                activeNano = new CellAO.Core.Nanos.ActiveNanoFunction(caster, target, ((NanoFormula)nano), func, (uint)funcIndex, null, DateTime.UtcNow);
                LogUtil.Debug(DebugInfoDetail.GameFunctions, "Function added to active nanos");
                // Some nanos include functions that need to be executed instantly
                if (func.InstantApplication)
                {
                    // Fire off the action first
                    activeNano.Tick();
                }
                // Some nanos include functions that can be applied during the next heartbeat and subsequently thereafter (Hots/Dots)

                // Queue it up for heartbeat to process
                target.ActiveNanos.Add(activeNano.Instance, (IActiveNanoFunction)activeNano);
                target.Stats[StatIds.currentncu].Value += ((NanoFormula)nano).NCUCost();


            }
            LogUtil.Debug(DebugInfoDetail.GameFunctions, "Sending SetNanoDuration");

            // CharacterAction 98 - Set nano duration
            target.Playfield.Announce(new CharacterActionMessage()
            {
                Identity = target.Identity,
                Unknown = 0,
                Action = CharacterActionType.SetNanoDuration,
                Target = new Identity() { Type = IdentityType.NanoProgram, Instance = ((NanoFormula)nano).ID},
                Parameter1 = activeNano.Instance,
                Parameter2 = (uint)((NanoFormula)nano).getItemAttribute((int)StatIds.timeexist)
            });
            /*
            CharacterActionMessageHandler.Default.SetNanoDuration(
                target,
                target.Identity, //new Identity() { Type = IdentityType.NanoProgram, Instance = activeNano.Instance },
                ((NanoFormula)nano).ID, 
                ((NanoFormula)nano).getItemAttribute((int)StatIds.timeexist));*/
        }

        public void CallFunction(Function function, IEntity caller)
        {
            FunctionCollection.Instance.CallFunction(
                function.FunctionType,
                this.Character,
                caller,
                this.Character,
                function.Arguments.Values.ToArray());
        }

        public void ReverseFunction(Function function, IEntity caller)
        {
            FunctionCollection.Instance.CallReverseFunction(
                function.FunctionType,
                this.Character,
                caller,
                this.Character,
                function.Arguments.Values.ToArray());
        }
    }
}