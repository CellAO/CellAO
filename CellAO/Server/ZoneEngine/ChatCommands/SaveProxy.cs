﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZoneEngine.ChatCommands
{
    using CellAO.Core.Entities;
    using CellAO.Core.Playfields;
    using CellAO.Core.Statels;
    using CellAO.Core.Vector;
    using CellAO.Database.Dao;
    using CellAO.Database.Entities;
    using CellAO.Enums;
    using CellAO.Interfaces;
    using CellAO.Messages.Fields;

    using ZoneEngine.Core.Functions.GameFunctions;
    using ZoneEngine.Core.MessageHandlers;
    using ZoneEngine.Core.Playfields;

    public class SaveProxy : AOChatCommand
    {
        public override bool CheckCommandArguments(string[] args)
        {
            return true;
        }

        public override void CommandHelp(ICharacter character)
        {

        }

        public override void ExecuteCommand(ICharacter character, Identity target, string[] args)
        {
            if ((character.Stats[StatIds.externaldoorinstance].Value == 0)
                || (character.Stats[StatIds.externalplayfieldinstance].Value == 0))
            {
                ChatTextMessageHandler.Default.Create(character, "Please enter a proxyfied playfield first.");
            }

            Coordinate tempCoordinate = character.Coordinates();
            PlayfieldData pfData = PlayfieldLoader.PFData[character.Playfield.Identity.Instance];
            StatelData o = null;
            foreach (StatelData s in pfData.Statels)
            {
                if (o == null)
                {
                    o = s;
                }
                else
                {
                    if (Coordinate.Distance2D(tempCoordinate, s.Coord())
                        < Coordinate.Distance2D(tempCoordinate, o.Coord()))
                    {
                        o = s;
                    }
                }
            }
            if (o == null)
            {

                ChatTextMessageHandler.Default.Create(
                    character,
                    "No statel on this playfield... Very odd, where exactly are you???");

            }
            else
            {
                DBTeleport tel = new DBTeleport();
                tel.Playfield = character.Stats[StatIds.externalplayfieldinstance].Value;
                tel.StatelType = 0xc748; // Door only for now
                tel.StatelInstance = character.Stats[StatIds.externaldoorinstance].BaseValue;
                tel.DestinationPlayfield = o.PlayfieldId;
                tel.DestinationType = (int)o.Identity.Type;
                tel.DestinationInstance = BitConverter.ToUInt32(BitConverter.GetBytes(o.Identity.Instance), 0);

                var temp = Dao<DBTeleport>.Instance.GetWhere(new { tel.Playfield, tel.StatelType, tel.StatelInstance });
                foreach (var t in temp)
                {
                    Dao<DBTeleport>.Instance.Delete(t.Id);
                }
                Dao<DBTeleport>.Instance.Add(tel);
                character.Playfield.Publish(
    ChatTextMessageHandler.Default.CreateIM(character, "Proxy saved"));

            }

        }

        public override int GMLevelNeeded()
        {
            return 1;
        }

        public override List<string> ListCommands()
        {
            return new List<string>() { "saveproxy" };
        }
    }
}
