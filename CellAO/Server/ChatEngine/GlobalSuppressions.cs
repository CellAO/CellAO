﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0017:Object initialization can be simplified", Justification = "<Pending>", Scope = "member", Target = "~M:ChatEngine.Relay.Common.IrcBot.Connect(System.String,IrcDotNet.IrcRegistrationInfo)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~M:ChatEngine.Relay.Common.IrcBot.client_ChannelListReceived(System.Object,IrcDotNet.IrcChannelListReceivedEventArgs)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:ChatEngine.Relay.Common.IrcBot.ReadChatCommand(IrcDotNet.IrcClient,IrcDotNet.IIrcMessageSource,System.Collections.Generic.IList{IrcDotNet.IIrcMessageTarget},System.String,System.String[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "<Pending>", Scope = "member", Target = "~M:ChatEngine.Relay.Common.IrcBot.ReadCommand(System.String,System.Collections.Generic.IList{System.String})")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~M:Chatengine.Relay.RelayBot.client_ChannelListReceived(System.Object,IrcDotNet.IrcChannelListReceivedEventArgs)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~P:ChatEngine.Channels.ChannelBase.channelType")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~P:ChatEngine.Channels.IChannelBase.channelType")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>", Scope = "member", Target = "~P:ChatEngine.CoreClient.Character.orgId")]
