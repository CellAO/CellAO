﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Enums
{
    public enum InitiativeType
    {
        PhysicalInit = 120,
        RangedInit = 119,
        MeleeInit = 118
    }
}
