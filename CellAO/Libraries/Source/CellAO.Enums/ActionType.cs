﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Enums
{
    /// <summary>
    /// </summary>
    public enum ActionType : int
    {
        Any = 0,
        Get = 1,
        Drop = 2,
        ToUse = 3,
        Repair = 4,
        UseItemOnItem = 5,
        ToWear = 6,
        ToRemove = 7,
        ToWield = 8,
        ToUnwield = 9,
        Split = 10,
        Attack = 11,
        AMS = 12,
        DMS = 13,
        DoubleAttack = 14,
        Idle = 15,
        CombatIdle = 16,
        Walk = 17,
        Run = 18,
        Sneak = 19,
        Crawl = 20,
        Burst = 22,
        AimedShot = 21,
        FullAuto = 23,
        LeftAttack = 24,
        FastAttack = 25,
        CombatIdleStart = 26,
        CombatIdleEnd = 27,
        FlingShot = 28,
        SneakAttack = 29,
        Terminate = 30,
        Impact = 31,
        UseItemOnCharacter = 32,
        LeftFoot = 33,
        RightFoot = 34,
        

        Open = 100,
        Close = 102,
        ToTriggerTargetInVicinity = 111,
        PlayShiftRequirements = 136,


    }
}