﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace Utility
{
    #region Usings ...

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    #endregion

    public static class DebugStrings
    {
        private const int columnWidth = 25;

        public static string DebugString(this UInt16 v)
        {
            return v.ToString().PadLeft(columnWidth) + " " + v.ToString("X4").PadRight(columnWidth) + v.GetType().Name;
        }

        public static string DebugString(this UInt16? v)
        {
            if (v == null)
            {
                return "null".PadRight(columnWidth * 2) + typeof(UInt16?).Name;
            }
            else
            {
                return ((UInt16)v).ToString().PadLeft(columnWidth) + " " + ((UInt16)v).ToString("X4").PadRight(columnWidth)
                       + v.GetType().Name;
            }
        }

        public static string DebugString(this Int16 v)
        {
            return v.ToString().PadLeft(columnWidth) + " " + v.ToString("X4").PadRight(columnWidth) + v.GetType().Name;
        }

        public static string DebugString(this Int16? v)
        {
            if (v == null)
            {
                return "null".PadRight(columnWidth * 2) + typeof(Int16?).Name;
            }
            else
            {
                return ((Int16)v).ToString().PadLeft(columnWidth) + " " + ((Int16)v).ToString("X4").PadRight(columnWidth)
                       + v.GetType().Name;
            }
        }

        public static string DebugString(this UInt32 v)
        {
            return v.ToString().PadLeft(columnWidth) + " " + v.ToString("X8").PadRight(columnWidth) + v.GetType().Name;
        }

        public static string DebugString(this UInt32? v)
        {
            if (v == null)
            {
                return "null".PadRight(columnWidth * 2) + typeof(UInt32?).Name;
            }
            else
            {
                return ((UInt32)v).ToString().PadLeft(columnWidth) + " " + ((UInt32)v).ToString("X8").PadRight(columnWidth)
                       + v.GetType().Name;
            }
        }

        public static string DebugString(this Int32 v)
        {
            return v.ToString().PadLeft(columnWidth) + " " + v.ToString("X8").PadRight(columnWidth) + v.GetType().Name;
        }

        public static string DebugString(this Int32? v)
        {
            if (v == null)
            {
                return "null".PadRight(columnWidth * 2) + typeof(Int32?).Name;
            }
            else
            {
                return ((Int32)v).ToString().PadLeft(columnWidth) + " " + ((Int32)v).ToString("X8").PadRight(columnWidth)
                       + v.GetType().Name;
            }
        }

        public static string DebugString(this string v)
        {
            if (v == null)
            {
                return "null".PadRight(columnWidth * 2) + typeof(string).Name;
            }
            return v.PadRight(columnWidth * 2) + v.GetType().Name;
        }

        public static string DebugString(this byte v)
        {
            return v.ToString().PadLeft(columnWidth) + " " + v.ToString("X2").PadRight(columnWidth) + v.GetType().Name;
        }

        public static string DebugString(this byte? v)
        {
            if (v == null)
            {
                return "null".PadRight(columnWidth * 2) + typeof(byte?).Name;
            }
            return v.ToString().PadLeft(columnWidth) + " " + ((byte)v).ToString("X2").PadRight(columnWidth) + v.GetType().Name;
        }

        public static string DebugString(this Single v)
        {
            return v.ToString("F").PadLeft(columnWidth) + " " + "".PadRight(columnWidth) + v.GetType().Name;
        }

        public static string DebugString(this Enum v)
        {
            Type t = Enum.GetUnderlyingType(v.GetType());
            if (t == typeof(byte))
            {
                return v.ToString().PadLeft(columnWidth) + " " + ((byte)Enum.Parse(v.GetType(), v.ToString())).ToString("X2").PadRight(columnWidth)
                       + v.GetType().Name;
            }
            else if (t == typeof(Int16))
            {
                return v.ToString().PadLeft(columnWidth) + " "
                       + ((Int16)Enum.Parse(v.GetType(), v.ToString())).ToString("X4").PadRight(columnWidth) + v.GetType().Name;
            }
            else if (t == typeof(Int32))
            {
                return v.ToString().PadLeft(columnWidth) + " "
                       + ((Int32)Enum.Parse(v.GetType(), v.ToString())).ToString("X8").PadRight(columnWidth) + v.GetType().Name;
            }
            else if (t == typeof(UInt16))
            {
                return v.ToString().PadLeft(columnWidth) + " "
                       + ((UInt16)Enum.Parse(v.GetType(), v.ToString())).ToString("X4").PadRight(columnWidth) + v.GetType().Name;
            }
            else if (t == typeof(UInt32))
            {
                return v.ToString().PadLeft(columnWidth) + " "
                       + ((UInt32)Enum.Parse(v.GetType(), v.ToString())).ToString("X8").PadRight(columnWidth) + v.GetType().Name;
            }
            else
            {
                return v.ToString().PadLeft(columnWidth * 2) + v.GetType().Name;
            }
        }

 
       
    }
}