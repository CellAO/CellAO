﻿#region License

// Copyright (c) 2005-2014, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace AOInject
{
    #region Usings ...

    using System;
    using CellAO.Messages;

    #endregion



    public interface IDataClass
    {
        DateTime TimeStamp { get; set; }

        byte[] DataBytes { get; set; }

        Message Message();

        string ToString();
    }
    public class JsonDataClass
    {
        public String formatted { get; set; }
        public DataClass data { get; set; }
    }
    [Serializable]
    public class DataClass : IDataClass
    {
        public int ProcessId;
        public DateTime TimeStamp { get; set; }

        public String Type
        {
            get
            {
                return this.Message() != null ? Message().Body.GetType().Name.ToString() : this.GetType().ToString();
            }
        }

        public String Direction
        {
            get
            {
                return ToClient ? "SC" : "CS";
            }
        }

        public byte[] DataBytes { get; set; }

        [NonSerialized]
        [Newtonsoft.Json.JsonProperty]
        public Message aotomationMessage = null;

        public bool ToClient { get; set; }

        public Message Message()
        {
            return this.aotomationMessage;
        }

        public DataClass(byte[] data, bool direction)
        {
            this.TimeStamp = DateTime.Now;
            this.DataBytes = data;
            this.aotomationMessage = null;
            this.ToClient = direction;
        }

        public DataClass()
        {
        }
        [System.Runtime.Serialization.OnDeserialized]
        internal void AfterJsonDeserialized(System.Runtime.Serialization.StreamingContext ctx)
        {
            var serializer = new CellAO.Messages.Messages.Serialization.MessageSerializer();
            try
            {
                this.aotomationMessage = serializer.Deserialize(new System.IO.MemoryStream(DataBytes));
            }
            catch (Exception ex)
            { // Do Nothing 
            }
        }

        public override string ToString()
        {
            string temp = "CS ";
            if (this.ToClient)
            {
                temp = "SC ";
            }
            if (this.aotomationMessage == null)
            {
                temp += "Unknown";
            }
            else
            {
                if (this.aotomationMessage.Body != null && this.aotomationMessage.Body is N3Message)
                {
                    temp += ((int)((N3Message)this.aotomationMessage.Body).N3MessageType).ToString("X8") + " "
                        + ((N3Message)this.aotomationMessage.Body).N3MessageType.ToString();
                }
            }
            return temp;
        }

        public string ToStringShort()
        {
            if (this.aotomationMessage == null)
            {
                return "Unknown";
            }
            if (this.aotomationMessage.Body == null)
            {
                return "Unknown";
            }
            if (!(this.aotomationMessage.Body is N3Message))
            {
                return "Unknown";
            }
            return ((N3Message)this.aotomationMessage.Body).N3MessageType.ToString();
        }
    }
}