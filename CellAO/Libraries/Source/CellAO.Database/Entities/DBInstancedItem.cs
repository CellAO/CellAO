﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database.Entities
{
    using System.Data.Linq;

    /// <summary>
    /// Data object for instanced items
    /// </summary>
    [Table("InstancedItems")]
    [Index("Key1", IndexAttribute.IndexTypeEnum.Unique)]
    public class DBInstancedItem : DBEntity
    {

        /// <summary>
        /// Instance id of the item
        /// </summary>
        [PrimaryKey]
        [Column(nullable: false, autoIncrement: true)]
        public override int Id { get; set; }


        /// <summary>
        /// Type id of the container
        /// </summary>
        [Column(nullable: false)]
        [Index("Key1")]
        public int ContainerType { get; set; }

        /// <summary>
        /// Instance of the container
        /// </summary>
        [Column(nullable: false)]
        [Index("Key1")]
        public int ContainerInstance { get; set; }

        /// <summary>
        /// Slot inside the container
        /// </summary>
        [Column(nullable: false)]
        [Index("Key1")]
        public int ContainerPlacement { get; set; }


        /// <summary>
        /// Type id of the instance
        /// </summary>
        [Column(nullable: false)]
        public int Itemtype { get; set; }

        /// <summary>
        /// Low Item id
        /// </summary>
        [Column(nullable: false)]
        public int LowId { get; set; }


        /// <summary>
        /// High item Id
        /// </summary>
        [Column(nullable: false)]
        public int HighId { get; set; }

        /// <summary>
        /// QL of the Item
        /// </summary>
        [Column(nullable: false)]
        public int Quality { get; set; }

        /// <summary>
        /// Multiple count stat
        /// </summary>
        [Column(nullable: false)]
        public int MultipleCount { get; set; }


        /// <summary>
        /// Coordinates (X)
        /// </summary>
        [Column(nullable: false)]
        public float X { get; set; }

        /// <summary>
        /// Coordinates (Y)
        /// </summary>
        [Column(nullable: false)]
        public float Y { get; set; }

        /// <summary>
        /// Coordinates (Z)
        /// </summary>
        [Column(nullable: false)]
        public float Z { get; set; }

        /// <summary>
        /// Heading (X)
        /// </summary>
        [Column(nullable: false)]
        public float HeadingX { get; set; }

        /// <summary>
        /// heading (Y)
        /// </summary>
        [Column(nullable: false)]
        public float HeadingY { get; set; }

        /// <summary>
        /// Heading (Z)
        /// </summary>
        [Column(nullable: false)]
        public float HeadingZ { get; set; }

        /// <summary>
        /// Heading (W) -  isn't that rotation angle ?
        /// </summary>
        [Column(nullable: false)]
        public float HeadingW { get; set; }


        /// <summary>
        /// Item's stats held as blob from msgpack serialization
        /// </summary>
        [Column]
        public Binary Stats { get; set; }
    }
}