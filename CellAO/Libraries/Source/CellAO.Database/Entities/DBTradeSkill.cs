﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database.Entities
{

    /// <summary>
    /// </summary>
    [Table("TradeSkill")]
    public class DBTradeSkill : DBEntity
    {
        [Column(nullable: false, length: 10)]
        public override int Id { get; set; }

        /// <summary>
        /// Source High ID
        /// </summary>
        [Column(nullable: false, length: 10)]
        public int ID1 { get; set; }

        /// <summary>
        /// Target High ID
        /// </summary>
        [Column(nullable: false)]
        public int ID2 { get; set; }

        /// <summary>
        /// For things like tier armor where the item must be at max QL to tradeskill. 0 = Any ql
        /// </summary>
        [Column(nullable: false, length: 10)]
        public int MinTarget { get; set; }

        /// <summary>
        /// Comma-delimited ID pairs
        /// </summary>
        [Column(nullable: false)]
        public string ResultIDS { get; set; }

        /// <summary>
        /// 0 = Do not check
        /// </summary>
        [Column(nullable: false, length: 10)]
        public int QLRangePercent { get; set; }

        /// <summary>
        /// Bit 0 = Delete src, Bit 1 = Delete target
        /// </summary>
        [Column(nullable: false, length: 10)]
        public int DeleteFlag { get; set; }

        /// <summary>
        /// Comma-delimited integers. ME = 125, EE = 126, Tutoring = 141, QFT = 157, WS = 158, PT = 159, NP = 160, CL = 161, Psy = 162, Chem = 163, B&E = 165, Strength = 16
        /// </summary>
        [Column(nullable: false)]
        public string Skill { get; set; }

        /// <summary>
        /// Comma-delimited integers. Skill required in % to build. 2.5x skill = 250
        /// </summary>
        [Column(nullable: false)]
        public string SkillPercent { get; set; }

        /// <summary>
        /// Comma-delimited integers. Bump by one QL for each one of these values above the requirement. e.g. Player has 650 skill. Item requires 550. This value is 50. Player has 100 extra points or two times this value. Bump by two QLs.
        /// </summary>
        [Column(nullable: false)]
        public string SkillPerBump { get; set; }

        /// <summary>
        /// Maximum number of one QL bumps allowed
        /// </summary>
        [Column(nullable: false, length: 10)]
        public int MaxBump { get; set; }

        [Column(nullable: false, length: 10)]
        public int MinXP { get; set; }

        [Column(nullable: false, length: 10)]
        public int MaxXP { get; set; }

        [Column(nullable: false, length: 10)]
        public int IsImplant { get; set; }

    }
}