﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database.Entities
{

    /// <summary>
    /// Data object for Login data
    /// </summary>
    [Table("Login")]
    [Index("Username", IndexAttribute.IndexTypeEnum.Unique)]
    public class DBLogin : DBEntity
    {

        [Column(nullable: false, autoIncrement: true)]
        [PrimaryKey]
        public override int Id { get; set; }

        /// <summary>
        /// Date of account creation
        /// </summary>
        [Column(nullable: false)]
        public System.DateTime CreationDate { get; set; }

        /// <summary>
        /// email address of the account
        /// </summary>
        [Column(nullable: false, length: 64)]
        public string Email { get; set; }

        /// <summary>
        /// Account owner first name
        /// </summary>
        [Column(nullable: false)]
        public string FirstName { get; set; }

        /// <summary>
        /// Account owner last name
        /// </summary>
        [Column(nullable: false)]
        public string LastName { get; set; }

        /// <summary>
        /// Account username
        /// </summary>
        [Column(nullable: false)]
        [Index("Username")]
        public string Username { get; set; }

        /// <summary>
        /// Password (hash)
        /// </summary>
        [Column(nullable: false, length: 100)]
        public string Password { get; set; }

        /// <summary>
        /// Number of allowed characters; 0 means no character allowed
        /// </summary>
        [Column(nullable: false, defaultValue: 6)]
        public int AllowedCharacters { get; set; }

        /// <summary>
        /// Account flags 2
        /// </summary>
        [Column(nullable: false)]
        public int Flags { get; set; }

        /// <summary>
        /// Account flags
        /// </summary>
        [Column(nullable: false)]
        public int AccountFlags { get; set; }

        /// <summary>
        /// Enabled Expansions (bitfield)
        /// </summary>
        [Column(nullable: false, defaultValue: 127)]
        public int Expansions { get; set; }

        /// <summary>
        /// GameMaster level 
        /// </summary>
        [Column(nullable: false)]
        public int GM { get; set; }



    }
}