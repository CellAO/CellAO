﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database
{
    using System;
    using System.Runtime.CompilerServices;

    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {

        public TableAttribute(string name, int autoIncrementFrom = 0)
        {
            this.Name = name;
            this.AutoIncrementFrom = autoIncrementFrom;
        }

        public string Name { get; private set; }
        public int AutoIncrementFrom { get; set; }

    }


    [AttributeUsage(AttributeTargets.Property)]
    public class PrimaryKeyAttribute : Attribute
    {

        public PrimaryKeyAttribute() {}

    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class IndexAttribute : Attribute
    {

        public enum IndexTypeEnum
        {
            Undefined,

            Unique
        }

        public IndexAttribute(string name, IndexTypeEnum indexType = IndexTypeEnum.Undefined)
        {
            this.Name = name;
            this.IndexType = indexType;
        }

        public string Name { get; set; }

        public IndexTypeEnum IndexType { get; set; }

    }
    
    

    /*
     * SMALLINT = 2 bytes (16 bit)  // from -32768 to 32768
     * INT = 4 bytes (32 bit)  // from -2147483648 to 2147483648 or from 0 to 4294967295
     * BIGINT = 8 bytes (64 bit)
     */
    public enum DBColumnType
    {
        UseReflection, // allows the Dao to re-evaluate the value depending on the property's type using reflection

        VarChar,
        BigInt,
        UnsignedBigInt,
        Int,
        UnsignedInt,
        SmallInt,
        UnsignedSmallInt,
        Float,
        DateTime,
        Bit,

        Binary
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ColumnAttribute : Attribute
    {
        private bool v1;
        private bool v2;

        public ColumnAttribute(bool v1, bool v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }

        public ColumnAttribute(DBColumnType dbType = DBColumnType.UseReflection, bool nullable = true, object defaultValue = null, int length = 0, [CallerMemberName] string name = null, bool autoIncrement = false, short float_M = 0, short float_D = 0, bool foreignKey = false)
        {
            this.DBColumnType = dbType;
            this.Nullable = nullable;
            this.ForeignKey = foreignKey;
            this.DefaultValue = defaultValue;
            this.Name = name;
            this.MaxLength = length;
            this.AutoIncrement = autoIncrement;
        }
        

        public bool Nullable { get; set; } // value will be set to false by the Dao if the current property is the primary key
        public bool ForeignKey { get; private set; }
        public object DefaultValue { get; protected set; }
        public string Name { get; set; } // value may be set by the Dao if not specified
        public DBColumnType DBColumnType { get; set; } // value may be set by the Dao uring reflection if set at design time to 'UseReflection' 
        public int MaxLength { get; set; }
        public bool AutoIncrement { get; set; }
        public short Float_M { get; set; } // only used for Float data types
        public short Float_D { get; set; } // only used for Float data types
    }
    
}
 