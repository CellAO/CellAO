﻿#region License

// Copyright (c) 2005-2016, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Database.Dao
{
    #region Usings ...

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reflection;

    using CellAO.Core.Exceptions;
    using CellAO.Database.Entities;

    using Dapper;
    using System.Text;
    using Utility;
    using System.Data.Linq;

    #endregion


    /// <summary>
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    /// <typeparam name="TU">
    /// </typeparam>
    //public class Dao<T, TU> : IDao<T>
    //    where T : IDBEntity, new() where TU : class, IDao<T>\
    public class Dao<T> : IDao<T>
    where T : IDBEntity, new()
    {

        private static Dao<T> _instance = null;
        /// <summary>
        /// Returns a generic Dao for a given DBentity
        /// </summary>
        public static Dao<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = Activator.CreateInstance<Dao<T>>(); 
                }
                return _instance;
            }
        }

        #region Static Fields

        /// <summary>
        /// </summary>
        private static Dictionary<string, PropertyInfo> cachedProperties = null;

        #endregion

        #region Public Properties

        /// <summary>
        /// </summary>
        /// <summary>
        /// </summary>
        public string TableName
        {
            get
            {
                return getTableName();
            }
        }


        #endregion

        #region Properties

        /// <summary>
        /// Cache the properties for fast access
        /// </summary>
        protected static Dictionary<string, PropertyInfo> CachedProperties
        {
            get
            {
                if (cachedProperties == null)
                {
                    cachedProperties = new Dictionary<string, PropertyInfo>();
                    lock (cachedProperties)
                    {
                        cachedProperties.Clear();
                        foreach (PropertyInfo property in
                            typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance))
                        {
                            cachedProperties.Add(property.Name, property);
                        }
                    }
                }

                return cachedProperties;
            }
        }

        #endregion


        #region Table-level methods

        public Type getType()
        {
            return typeof(T);
        }

        public PropertyInfo[] getProperties()
        {
            return getType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
        }

        public string getTableName()
        {
            var atts = getType().GetCustomAttributes(typeof(TableAttribute), false);
            if (atts.Length == 0)
            {
                return getType().Name; // use class name
            }
            else
            {
                return ((TableAttribute)atts[0]).Name;
            }

        }

        public IEnumerable<string> getPrimaryKeys(bool throwErrorIfNotFound = false)
        {
            // finds the property with the PrimaryKeyAttribute
            var primaryKeys = getProperties()
                .Where(p => p.GetCustomAttributes(typeof(PrimaryKeyAttribute), false).Any())
                .Select(p => p.Name);


            if (primaryKeys.Count() == 0 && throwErrorIfNotFound)
                Utility.LogUtil.Debug(Utility.DebugInfoDetail.Database, $"The DB entity class {getType().Name} has no property decorated with [PrimaryKey]. Is it by design ?");

            return primaryKeys;

        }

        public IEnumerable<ColumnAttribute> GetColumns()
        {
            List<ColumnAttribute> columns = new List<ColumnAttribute>();
            // finds the property with the PrimaryKeyAttribute
            foreach(var prop in getProperties().Where(p => p.GetCustomAttributes(typeof(ColumnAttribute), false).Any()))
            {
                var c = prop.GetCustomAttribute<ColumnAttribute>();

                if (c.DBColumnType == DBColumnType.UseReflection)
                {
                    c.DBColumnType = GetDbTypeWithReflection(prop);
                }

                columns.Add(c);

            }
            return columns;

        }

        public IEnumerable<string> AutoIncrementColumns{
            get
            {
                return GetColumns().Where(c => c.AutoIncrement).Select(c => c.Name);
            }
        }


        public struct IndexInfo
        {
            public string Name { get; set; }
            public List<string> Columns { get; set; }

            public IndexAttribute.IndexTypeEnum IndexType { get; set; }
        }


        protected Dictionary<string, IndexInfo> getIndexes()
        {

            // is class decorated with the Index attribute ?
            var indexClassAtts = getType().GetCustomAttributes(typeof(IndexAttribute), false);

            if (indexClassAtts.Length == 0)
            {
                return null; // use class name
            }

            // fills a dictionary for each index defined at the class level
            Dictionary<string, IndexInfo> indexes = new Dictionary<string, IndexInfo>();
            foreach (var att in indexClassAtts)
            {
                var indexAtt = (IndexAttribute)att;

                if (!indexes.ContainsKey(indexAtt.Name))
                {
                    indexes.Add(indexAtt.Name, new IndexInfo() { Name = indexAtt.Name, IndexType = indexAtt.IndexType, Columns = new List<string>() });
                }
            }

            // fills the columns lists for each index, by parsing each property that has been tagged with the Index attribute
            foreach (var property in getProperties())
            {
                var atts = property.GetCustomAttributes(typeof(IndexAttribute), false);
                if (atts.Length > 0)
                {
                    foreach (var att in atts)
                    {
                        var indexAtt = (IndexAttribute)att;
                        if (!indexes.ContainsKey(indexAtt.Name))
                        {
                            Utility.LogUtil.Debug(Utility.DebugInfoDetail.Database, $"The index '{indexAtt.Name}' declared on the property '{property.Name}' must be defined at the class level. It will be ignored for now.");
                            continue;
                        }

                        indexes[indexAtt.Name].Columns.Add(property.Name);
                    }
                }
            }

            return indexes;
        }

        protected ColumnAttribute getColumnAttributeForProperty(PropertyInfo prop, bool throwErrorIfNotFound = false)
        {
            var atts = prop.GetCustomAttributes(typeof(ColumnAttribute), false);
            if (atts.Length > 1)
                throw new DataBaseException($"The property '{prop.Name}' in class '{getTableName()}' should have exatctly one [Column] attribute.");

            if (atts.Length == 1) 
                return (ColumnAttribute)atts[0];   

            if (throwErrorIfNotFound)
                throw new DataBaseException($"The property '{prop.Name}' in class '{getTableName()}' is not decorated with the [Column] attribute.");
            else
                return null;
        }

        protected PrimaryKeyAttribute getPrimaryKeyAttributeForProperty(PropertyInfo prop, bool throwErrorIfNotFound = false)
        {
            var atts = getType().GetCustomAttributes(typeof(PrimaryKeyAttribute), false);
            if (atts.Length == 1)
            {
                return (PrimaryKeyAttribute)atts[0];
            }

            if (throwErrorIfNotFound)
                throw new DataBaseException($"The property {prop.Name} in class '{getTableName()}' is not decorated with the [PrimaryKey] attribute.");

            return null;
        }

        private DBColumnType GetDbTypeWithReflection(PropertyInfo prop)
        {
            if (prop.PropertyType == typeof(string))
                return DBColumnType.VarChar;

            if (prop.PropertyType == typeof(bool))
                return DBColumnType.Bit;

            if (prop.PropertyType == typeof(DateTime))
                return DBColumnType.DateTime;

            if (prop.PropertyType == typeof(short))
                return DBColumnType.SmallInt;

            if (prop.PropertyType == typeof(ushort))
                return DBColumnType.UnsignedSmallInt;

            if (prop.PropertyType == typeof(int))
                return DBColumnType.Int;

            if (prop.PropertyType == typeof(uint))
                return DBColumnType.UnsignedInt;

            if (prop.PropertyType == typeof(long))
                return DBColumnType.BigInt;
            if (prop.PropertyType == typeof(ulong))
                return DBColumnType.UnsignedBigInt;

            if (prop.PropertyType == typeof(float))
                return DBColumnType.Float;

            if (prop.PropertyType == typeof(Binary))
                return DBColumnType.Binary;

            // Nullables
            if (prop.PropertyType == typeof(string))
                return DBColumnType.VarChar;

            if (prop.PropertyType == typeof(bool?))
                return DBColumnType.Bit;

            if (prop.PropertyType == typeof(DateTime?))
                return DBColumnType.DateTime;

            if (prop.PropertyType == typeof(short?))
                return DBColumnType.SmallInt;

            if (prop.PropertyType == typeof(ushort?))
                return DBColumnType.UnsignedSmallInt;

            if (prop.PropertyType == typeof(int?))
                return DBColumnType.Int;

            if (prop.PropertyType == typeof(uint?))
                return DBColumnType.UnsignedInt;

            if (prop.PropertyType == typeof(long?))
                return DBColumnType.BigInt;
            if (prop.PropertyType == typeof(ulong?))
                return DBColumnType.UnsignedBigInt;

            if (prop.PropertyType == typeof(float?))
                return DBColumnType.Float;

            throw new DataBaseException($"No DBColumnType value defined for property {prop.Name} in class '{getTableName()}' with type '{prop.PropertyType.FullName}'");
        }

        private string getDbTypeAsString(DBColumnType columnType)
        {
            switch (columnType)
            {
                case DBColumnType.Bit:
                    return "BIT";
                case DBColumnType.BigInt:
                case DBColumnType.UnsignedBigInt:
                    return "BIGINT";
                case DBColumnType.Int:
                case DBColumnType.UnsignedInt:
                    return "INT";
                case DBColumnType.SmallInt:
                case DBColumnType.UnsignedSmallInt:
                    return "SMALLINT";
                case DBColumnType.Float:
                    return "FLOAT";
                case DBColumnType.DateTime:
                    return "DATETIME";
                case DBColumnType.VarChar:
                    return "VARCHAR"; // A VARCHAR should always be used even instead of TINYTEXT. 
                case DBColumnType.Binary:
                    return "BLOB";

                default:
                    throw new DataBaseException($"No string value defined for type '{Enum.GetName(typeof(DBColumnType), columnType)}' in class in class '{getTableName()}'. Check all properties with [Column] attributes.");
            }
        }

        private string getDefaultValueAsStringForDbType(DBColumnType columnType)
        {
            switch (columnType)
            {
                case DBColumnType.UseReflection:
                    throw new NotImplementedException();

                case DBColumnType.Bit:
                case DBColumnType.BigInt:
                case DBColumnType.UnsignedBigInt:
                case DBColumnType.Int:
                case DBColumnType.UnsignedInt:
                case DBColumnType.SmallInt:
                case DBColumnType.UnsignedSmallInt:
                case DBColumnType.Float:
                    return "0";
                case DBColumnType.DateTime:
                    return DateTime.MinValue.ToShortDateString();
                case DBColumnType.VarChar:
                    return "";
                case DBColumnType.Binary:
                    return null; // no default value for BLOBs

                default:
                    throw new DataBaseException($"No default value defined for type '{Enum.GetName(typeof(DBColumnType), columnType)}' in class in class '{getTableName()}'. Please check all properties marked with the [Column] attribute.");
            }
        }

        private const int DEFAULT_VARCHAR_MAXLENGTH = 32;

        private int getMaxLengthForDbType(DBColumnType columnType)
        {
            switch (columnType)
            {
                case DBColumnType.BigInt:
                case DBColumnType.UnsignedBigInt:
                    return 64;

                case DBColumnType.Int:
                case DBColumnType.UnsignedInt:
                    return 32;

                case DBColumnType.SmallInt:
                case DBColumnType.UnsignedSmallInt:
                    return 16;

                case DBColumnType.VarChar:
                    return DEFAULT_VARCHAR_MAXLENGTH;

                default:
                    return -1; // pointless
            }

        }

        protected string getCreateTableSql()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"CREATE TABLE `{getTableName()}` (");
            // TODO : use the GetColumns() method down here...
            foreach (var prop in getProperties())
            {
                var col = getColumnAttributeForProperty(prop);
                if (col == null) continue;

                if (string.IsNullOrEmpty(col.Name))
                    col.Name = prop.Name;

                if (col == null)
                    continue; // skip properties not decorated with [Column] attribute

                sb.Append($" `{prop.Name}` ");

                // data type
                if (col.DBColumnType == DBColumnType.UseReflection)
                    col.DBColumnType = GetDbTypeWithReflection(prop);

                sb.Append(getDbTypeAsString(col.DBColumnType));

                // data type length
                var maxLength = col.MaxLength;
                if (maxLength == 0)
                {
                    // get the default maxLength/precision from the db type if not explicitely defined
                    maxLength = getMaxLengthForDbType(col.DBColumnType);
                }
                if ((maxLength > 0) && (col.DBColumnType != DBColumnType.Float))
                    sb.Append($"({maxLength}) ");

                if ((col.DBColumnType == DBColumnType.Float) &&
                        ((col.Float_M != 0) || (col.Float_D != 0)))
                    sb.Append($"({col.Float_M},{col.Float_D}) ");

                // data type unsigned keyword
                if ((col.DBColumnType == DBColumnType.UnsignedInt) || (col.DBColumnType == DBColumnType.UnsignedSmallInt))
                    sb.Append(" unsigned ");

                // primary keys
                var isPrimaryKey = getPrimaryKeys().Contains(col.Name);
                if (isPrimaryKey)
                    col.Nullable = false; // primary keys are always non-nullable

                sb.Append(col.Nullable ? " NULL " : " NOT NULL ");

                if (col.AutoIncrement)
                    if (!isPrimaryKey)
                        throw new DataBaseException($"Property {prop.Name} in class {getTableName()} must be marked with the [PrimaryKey] attribute if it is an auto-incrementing column.");
                    else
                        sb.Append(" AUTO_INCREMENT "); // we don't consider here primary keys that are *not* auto-incremented

                // if (!getPrimaryKeys().Contains(col.Name) && !col.AutoIncrement)
                if (!(col.Nullable || col.AutoIncrement))
                {
                    // must have a default value defined 

                    var defaultValue = getDefaultValueAsStringForDbType(col.DBColumnType);
                    if (!string.IsNullOrEmpty(defaultValue))
                        sb.Append($" DEFAULT '{defaultValue}' ");
                }
                sb.Append(","); // trailing coma
            }

            if (getPrimaryKeys().Count() > 0)
            {
                sb.Append($" PRIMARY KEY (");
                foreach (var pk in getPrimaryKeys())
                {

                    sb.Append($"`{pk}`");
                    sb.Append(","); // trailing coma
                }
                sb.Remove(sb.Length - 1, 1); // remove trailing coma
                sb.Append("),");
            }

            // define indexes
            // UNIQUE INDEX `Username` (`Username`)
            var indexes = getIndexes();
            if (indexes != null)
            {
                foreach (var index in indexes)
                {
                    var info = (IndexInfo)index.Value;
                    if (info.IndexType == IndexAttribute.IndexTypeEnum.Unique)
                        sb.Append($" UNIQUE INDEX `{info.Name}` (");
                    else
                        sb.Append($" INDEX `{info.Name}` ("); // not sure about this, but meh...

                    foreach (var col in info.Columns)
                    {
                        sb.Append($" `{col}` ");
                        sb.Append(","); // trailing coma
                    }
                    sb.Remove(sb.Length - 1, 1); // remove trailing coma

                    sb.Append("),"); // trailing coma
                }
            }

            sb.Remove(sb.Length - 1, 1); // remove trailing coma
            sb.Append(")"); // CREATE TABLE

            // TODO: If we are using MySQL, then add the following
            if (Config.Current.SQLType.ToLower() == "mysql")
                sb.Append(" ENGINE=InnoDB  DEFAULT CHARSET=latin1 ");

            // if AutoIncrementFrom is defined at the TableAttribute level, specifies a value for AUTO_INCREMENT
            var tableAtt = (TableAttribute)(getType().GetCustomAttributes(typeof(TableAttribute), false)[0]);

            if (tableAtt.AutoIncrementFrom != 0)
                sb.Append($" AUTO_INCREMENT={tableAtt.AutoIncrementFrom}");

            sb.Append(";");
            return sb.ToString();
        }

        public void CreateTable(bool createIfExists = false)
        {
            using (var conn = Connector.GetConnection())
            {
                if (TableExists())
                {
                    if (!createIfExists)
                    {
                        Utility.LogUtil.Debug(Utility.DebugInfoDetail.Database, $"Table '{getTableName()}' already exists and will not be overriden.");
                        return;
                    }

                }
                var sql = getCreateTableSql();
                conn.Execute(sql);

            }
        }

        public bool TableExists()
        {
            using (var conn = Connector.GetConnection())
            {
                return conn.Query<string>($"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{getTableName()}' and TABLE_SCHEMA = '{conn.Database}'").Count() == 1;
            }

        }

        public void DeleteTable(bool yeahIReallyWannaDoThat = false)
        {
            if (!yeahIReallyWannaDoThat) return;

            using (var conn = Connector.GetConnection())
            {
                conn.Execute($"DROP TABLE `{getTableName()}`");
            }
        }

        public void TruncateTable(bool yeahIReallyWannaDoThat = false)
        {
            if (!yeahIReallyWannaDoThat) return;

            using (var conn = Connector.GetConnection())
            {
                conn.Execute($"TRUNCATE TABLE `{getTableName()}`");
            }
        }

        public bool HasRecords()
        {
            using (var conn = Connector.GetConnection())
            {
                return conn.ExecuteScalar<int>($"SELECT COUNT(*) FROM `{getTableName()}`") > 0;
            }

        }

        #endregion


        #region CRUD Methods

        /// <summary>
        /// </summary>
        /// <param name="entity">
        /// </param>
        /// <param name="connection">
        /// </param>
        /// <param name="transaction">
        /// </param>
        /// <returns>
        /// </returns>
        /// <exception cref="DataBaseException">
        /// </exception>
        public int Add(
            T entity,
            IDbConnection connection = null,
            IDbTransaction transaction = null)
        {
            int rowsAffected = 0;

            IDbConnection conn = connection;
            try
            {
                conn = conn ?? Connector.GetConnection();
                IDbTransaction trans = transaction;
                try
                {
                    trans = trans ?? conn.BeginTransaction();
                    
                    string sql = SqlMapperUtil.CreateInsertSQL(this.TableName, entity, this.AutoIncrementColumns);
                    rowsAffected = conn.Execute(
                        sql,
                        entity,
                        trans);

                    if (rowsAffected != 1)
                        throw new DataBaseException(
                            string.Format("Failed to create new record on table '{0}'", this.TableName));

                    SqlMapperUtil.GetIdentity<int>(conn, 
                        id => 
                        entity.Id = ( (id == 0) ? entity.Id : id) // set the identity Id if required (in the case of an autonumber)
                        , trans);

                }
                finally
                {
                    if (transaction == null)
                    {
                        if (trans != null)
                        {
                            trans.Commit();
                            trans.Dispose();
                        }
                    }
                }
            }
            finally
            {
                if (connection == null)
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
            }

            return rowsAffected;
        }

        public int Add(
            List<object> entities,
            object parameters = null,
            IDbConnection connection = null,
            IDbTransaction transaction = null)

        {
            List<T> conv = new List<T>();
            foreach(var o in entities)
                conv.Add((T)Convert.ChangeType(o, typeof(T))); 

            return this.Add(conv, parameters, connection, transaction);
        }

        /// <summary>
        /// </summary>
        /// <param name="entities">
        /// </param>
        /// <param name="parameters">
        /// </param>
        /// <param name="connection">
        /// </param>
        /// <param name="transaction">
        /// </param>
        /// <returns>
        /// </returns>
        public int Add(
            List<T> entities,
            object parameters = null,
            IDbConnection connection = null,
            IDbTransaction transaction = null)
        {
            int rowsAffected = 0;

            IDbConnection conn = connection;
            try
            {
                conn = conn ?? Connector.GetConnection();
                IDbTransaction trans = transaction;
                try
                {
                    trans = trans ?? conn.BeginTransaction();
                    foreach (T entity in entities)
                    {
                        rowsAffected += Add(entity, conn, trans); // Pass parameters instead of null here? 
                    }
                }
                finally
                {
                    if (transaction == null)
                    {
                        if (trans != null)
                        {
                            trans.Commit();
                            trans.Dispose();
                        }
                    }
                }
            }
            finally
            {
                if (connection == null)
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
            }

            return rowsAffected;
        }

        /// <summary>
        /// Delete a row from the table
        /// </summary>
        /// <param name="entityId">
        /// id of the row to delete
        /// </param>
        /// <param name="connection">
        /// </param>
        /// <param name="transaction">
        /// optional transaction for the delete
        /// </param>
        /// <returns>
        /// </returns>
        public int Delete(int entityId, IDbConnection connection = null, IDbTransaction transaction = null)
        {
            int rowsAffected = 0;
            IDbConnection conn = connection;
            try
            {
                conn = conn ?? Connector.GetConnection();
                IDbTransaction trans = transaction;
                try
                {
                    trans = trans ?? conn.BeginTransaction();
                    rowsAffected = conn.Execute(
                        SqlMapperUtil.CreateDeleteSQL(this.TableName),
                        new { id = entityId },
                        transaction);
                }
                finally
                {
                    if (transaction == null)
                    {
                        if (trans != null)
                        {
                            trans.Commit();
                            trans.Dispose();
                        }
                    }
                }
            }
            finally
            {
                if (connection == null)
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
            }

            return rowsAffected;
        }

        /// <summary>
        /// Delete rows from table
        /// </summary>
        /// <param name="whereParameters">
        /// optional DynamicParameters with the items for the where-clause
        /// </param>
        /// <param name="connection">
        /// </param>
        /// <param name="transaction">
        /// optional transaction for the delete
        /// </param>
        /// <returns>
        /// </returns>
        public int Delete(object whereParameters, IDbConnection connection = null, IDbTransaction transaction = null)
        {
            int rowsAffected = 0;
            IDbConnection conn = connection;
            try
            {
                conn = conn ?? Connector.GetConnection();
                IDbTransaction trans = transaction;
                try
                {
                    trans = trans ?? conn.BeginTransaction();
                    rowsAffected = conn.Execute(
                        SqlMapperUtil.CreateDeleteSQL(this.TableName, whereParameters),
                        whereParameters,
                        transaction);
                }
                finally
                {
                    if (transaction == null)
                    {
                        if (trans != null)
                        {
                            trans.Commit();
                            trans.Dispose();
                        }
                    }
                }
            }
            finally
            {
                if (connection == null)
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
            }

            return rowsAffected;
        }

        /// <summary>
        /// </summary>
        /// <param name="entityId">
        /// </param>
        /// <returns>
        /// </returns>
        public bool Exists(int entityId)
        {
            bool exists = false;
            using (IDbConnection conn = Connector.GetConnection())
            {
                exists =
                    conn.Query<int>(
                        string.Format("SELECT ID FROM {0} where ID = @id", this.TableName),
                        new { id = entityId }).Count() == 1;
            }

            return exists;
        }

        /// <summary>
        /// Load a Character by id
        /// </summary>
        /// <param name="entityId">
        /// Id of the Character
        /// </param>
        /// <returns>
        /// DBCharacter object
        /// </returns>
        public T Get(int entityId)
        {
            T entity = default(T);
            using (IDbConnection conn = Connector.GetConnection())
            {
                entity =
                    conn.Query<T>(
                        SqlMapperUtil.CreateGetSQL(this.TableName, new { Id = entityId }),
                        new { Id = entityId }).SingleOrDefault();
            }

            return entity;
        }

        /// <summary>
        /// Load all Character data
        /// </summary>
        /// <param name="parameters">
        /// </param>
        /// <returns>
        /// Collection of DBCharacter
        /// </returns>
        public IEnumerable<T> GetAll(object parameters = null)
        {
            IEnumerable<T> entities = null;
            using (IDbConnection conn = Connector.GetConnection())
            {
                entities = conn.Query<T>(SqlMapperUtil.CreateGetSQL(this.TableName, parameters), parameters);
            }

            return entities;
        }

        /// <summary>
        /// </summary>
        /// <param name="entity">
        /// </param>
        /// <param name="parameters">
        /// </param>
        /// <param name="connection">
        /// </param>
        /// <param name="transaction">
        /// </param>
        /// <returns>
        /// </returns>
        public int Save(
            T entity,
            object parameters = null,
            IDbConnection connection = null,
            IDbTransaction transaction = null)
        {
            int rowsAffected = 0;

            IDbConnection conn = connection;
            try
            {
                conn = conn ?? Connector.GetConnection();
                IDbTransaction trans = transaction;
                try
                {
                    trans = trans ?? conn.BeginTransaction();
                    
                    rowsAffected = conn.Execute(
                        SqlMapperUtil.CreateUpdateSQL(this.TableName, parameters ?? entity, this.AutoIncrementColumns),
                        parameters ?? entity,
                        transaction);
                }
                finally
                {
                    if (transaction == null)
                    {
                        if (trans != null)
                        {
                            trans.Commit();
                            trans.Dispose();
                        }
                    }
                }
            }
            finally
            {
                if (connection == null)
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
            }

            return rowsAffected;
        }

        /// <summary>
        /// </summary>
        /// <param name="entities">
        /// </param>
        /// <param name="parameters">
        /// </param>
        /// <param name="connection">
        /// </param>
        /// <param name="transaction">
        /// </param>
        /// <returns>
        /// </returns>
        public int Save(
            List<T> entities,
            object parameters = null,
            IDbConnection connection = null,
            IDbTransaction transaction = null)
        {
            int rowsAffected = 0;

            IDbConnection conn = connection;
            try
            {
                conn = conn ?? Connector.GetConnection();
                IDbTransaction trans = transaction;
                try
                {
                    trans = trans ?? conn.BeginTransaction();
                    foreach (T entity in entities)
                    {
                        rowsAffected += Save(entity, null, conn, trans); // Pass parameters instead of null here? 
                    }
                }
                finally
                {
                    if (transaction == null)
                    {
                        if (trans != null)
                        {
                            trans.Commit();
                            trans.Dispose();
                        }
                    }
                }
            }
            finally
            {
                if (connection == null)
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
            }

            return rowsAffected;
        }

        #endregion

        #region Misc Methods

        /// <summary>
        /// </summary>
        /// <param name="item">
        /// </param>
        /// <returns>
        /// </returns>
        private DynamicParameters getAllParameters(T item)
        {
            DynamicParameters parameters = new DynamicParameters();
            foreach (string propertyName in CachedProperties.Keys)
            {
                parameters.Add(propertyName, CachedProperties[propertyName].GetValue(item, null));
            }

            return parameters;
        }

        public IEnumerable<T> GetWhere(
            object parameter = null,
            IDbConnection connection = null,
            IDbTransaction transaction = null)
        {
            IEnumerable<T> result = new List<T>();

            IDbConnection conn = connection;
            try
            {
                conn = conn ?? Connector.GetConnection();
                IDbTransaction trans = transaction;
                try
                {
                    result = conn.Query<T>(SqlMapperUtil.CreateGetSQL(this.TableName, parameter), parameter, trans);
                }
                finally
                {
                    if (transaction == null)
                    {
                        if (trans != null)
                        {
                            trans.Commit();
                            trans.Dispose();
                        }
                    }
                }
            }
            finally
            {
                if (connection == null)
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
            }
            return result;
        }

        public long Count(object parameter = null, IDbConnection connection = null, IDbTransaction transaction = null)
        {
            long result = -1;
            IDbConnection conn = connection;
            try
            {
                conn = conn ?? Connector.GetConnection();
                IDbTransaction trans = transaction;
                try
                {
                    result = conn.Query<long>(SqlMapperUtil.CreateCountSQL(this.TableName, parameter), trans).Single();
                }
                finally
                {
                    if (transaction == null)
                    {
                        if (trans != null)
                        {
                            trans.Commit();
                            trans.Dispose();
                        }
                    }
                }
            }
            finally
            {
                if (connection == null)
                {
                    if (conn != null)
                    {
                        conn.Dispose();
                    }
                }
            }
            return result;
        }

        #endregion

    }
}