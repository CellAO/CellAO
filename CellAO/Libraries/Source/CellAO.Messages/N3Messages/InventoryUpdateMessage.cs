﻿namespace CellAO.Messages.N3Messages
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;
    #region Usings ...

    using CellAO.Messages.Fields;

    #endregion

    /// <summary>
    /// </summary>
    [AoContract((int)N3MessageType.InventoryUpdate)]
    public class InventoryUpdateMessage : N3Message
    {
        public InventoryUpdateMessage()
        {
            this.N3MessageType = N3MessageType.InventoryUpdate;
        }

        /// <summary>
        /// </summary>
        [MessageField(0)]
        public int NumberOfSlots { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(1)]
        // 0x00000003
        public int Unknown1 { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(2, SerializeSize = ArraySizeType.X3F1)]
        public InventoryEntry[] Entries { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(3)]
        public Identity BagIdentity { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(4)]
        public int SlotnumberInMainInventory { get; set; }

        /// <summary>
        /// </summary>
        [MessageField(5)]
        // 0x00000001
        public int Unknown2 { get; set; }
    }
}