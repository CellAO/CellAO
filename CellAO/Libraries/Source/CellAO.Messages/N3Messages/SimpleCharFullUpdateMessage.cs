﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SimpleCharFullUpdateMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the SimpleCharFullUpdateMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Enums;
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.Fields;

    [AoContract((int)N3MessageType.SimpleCharFullUpdate)]
    public class SimpleCharFullUpdateMessage : N3Message
    {
        #region Constructors and Destructors

        public SimpleCharFullUpdateMessage()
        {
            this.N3MessageType = N3MessageType.SimpleCharFullUpdate;
            this.Unknown = 0x00;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public byte Version { get; set; }

        [MessageField(1)]
        public SimpleCharFullUpdateFlags Flags { get; set; }

        [MessageField(2)]
        public int? PlayfieldId { get; set; }

        [MessageField(3)]
        public Identity? FightingTarget { get; set; }

        [MessageField(4)]
        public Vector3 Coordinates { get; set; }

        [MessageField(5)]
        public Quaternion Heading { get; set; }

        [MessageField(6)]
        public Appearance Appearance { get; set; }

        [MessageField(7)]
        public string Name { get; set; }

        [MessageField(8)]
        public CharacterFlags CharacterFlags { get; set; }

        [MessageField(9)]
        public short AccountFlags { get; set; }

        [MessageField(10)]
        public short Expansions { get; set; }

        [MessageField(11)]
        public SimpleCharacterInfo CharacterInfo { get; set; }

        [MessageField(12)]
        public short Level { get; set; }

        [MessageField(13)]
        public int Health { get; set; }

        [MessageField(14)]
        public int HealthDamage { get; set; }

        [MessageField(15)]
        public uint MonsterData { get; set; }

        [MessageField(16)]
        public short MonsterScale { get; set; }

        [MessageField(17)]
        public short VisualFlags { get; set; }

        [MessageField(18)]
        public byte VisibleTitle { get; set; }

        [MessageField(19, SerializeSize = ArraySizeType.Int32)]
        public byte[] Unknown1 { get; set; }

        [MessageField(20)]
        public uint? HeadMesh { get; set; }

        [MessageField(21)]
        public short RunSpeedBase { get; set; }

        [MessageField(22, SerializeSize = ArraySizeType.X3F1)]
        public ActiveNano[] ActiveNanos { get; set; }

        [MessageField(23, SerializeSize = ArraySizeType.X3F1)]
        public Texture[] Textures { get; set; }

        [MessageField(24, SerializeSize = ArraySizeType.X3F1)]
        public Mesh[] Meshes { get; set; }

        [MessageField(25)]
        public int Flags2 { get; set; }

        [MessageField(26)]
        public byte Unknown2 { get; set; }

        #endregion
    }
}