﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.N3Messages
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.Action)]
    public class ActionMessage : N3Message
    {
        public ActionMessage()
        {
            this.N3MessageType = N3MessageType.Action;
        }

        [MessageField(1)]
        [AoFlags("flag")]
        public int Unknown1 { get; set; }

        [AoUsesFlags("flag", typeof(int), FlagsCriteria.HasAny, new[] { 1 })]
        [MessageField(2)]
        public int Unknown2 { get; set; }

        [AoUsesFlags("flag", typeof(Identity), FlagsCriteria.HasAny, new[] { 1 })]
        [MessageField(3)]
        public Identity Unknown3 { get; set; }

    }
}
