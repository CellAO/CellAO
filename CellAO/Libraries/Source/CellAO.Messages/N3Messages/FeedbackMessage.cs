﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FeedbackMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the FeedbackMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using CellAO.Interfaces.Attributes;

namespace CellAO.Messages.N3Messages
{

    [AoContract((int)N3MessageType.Feedback)]
    public class FeedbackMessage : N3Message
    {
        #region Constructors and Destructors

        public FeedbackMessage()
        {
            this.N3MessageType = N3MessageType.Feedback;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public int Unknown1 { get; set; }

        [MessageField(1)]
        public int CategoryId { get; set; }

        [MessageField(2)]
        public int MessageId { get; set; }

        #endregion
    }
}