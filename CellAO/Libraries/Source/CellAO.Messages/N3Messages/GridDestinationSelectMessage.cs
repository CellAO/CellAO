﻿namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.GridDestinationSelect)]
    class GridDestinationSelectMessage : N3Message
    {
        public GridDestinationSelectMessage()
        {
            N3MessageType = N3MessageType.GridDestinationSelect;
        }
    }
}