﻿using CellAO.Interfaces;
using CellAO.Interfaces.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Messages.N3Messages
{
    [AoContract((int)N3MessageType.MoveItemToBag)]
    public class MoveItemToBagMessage : N3Message
    {
        [MessageField(1)] public Identity BagIdentity { get; set; }
        [MessageField(2)] public Identity SourceIdentity { get; set; }
        [MessageField(3)] public byte Unknown3 { get; set; }
        [MessageField(4)] public byte Unknown4 { get; set; }
        [MessageField(5)] public byte Unknown5 { get; set; }

        public MoveItemToBagMessage()
        {
            this.N3MessageType = N3MessageType.MoveItemToBag;
        }
    }
}
