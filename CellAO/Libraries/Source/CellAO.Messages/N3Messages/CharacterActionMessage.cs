﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CharacterActionMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the CharacterActionMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.CharacterAction)]
    public class CharacterActionMessage : N3Message
    {
        #region Constructors and Destructors

        public CharacterActionMessage()
        {
            this.N3MessageType = N3MessageType.CharacterAction;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public CharacterActionType Action { get; set; }

        [MessageField(1)]
        public int Unknown1 { get; set; }

        [MessageField(2)]
        public Identity Target { get; set; }

        [MessageField(3)]
        public int Parameter1 { get; set; }

        [MessageField(4)]
        public uint Parameter2 { get; set; }

        [MessageField(5)]
        public short Unknown2 { get; set; }

        #endregion
    }
}