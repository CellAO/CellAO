﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SpecialAttackWeaponMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the SpecialAttackWeaponMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.Fields;

    [AoContract((int)N3MessageType.SpecialAttackWeapon)]
    public class SpecialAttackWeaponMessage : N3Message
    {
        #region Constructors and Destructors

        public SpecialAttackWeaponMessage()
        {
            this.N3MessageType = N3MessageType.SpecialAttackWeapon;
            this.Unknown1 = 0x00000007;
            this.Unknown2 = 0x00000007;
            this.Unknown3 = 0x00000007;
            this.Unknown4 = 0x0000000E;
            this.AggDef= 0x00000064;
        }

        #endregion

        #region AoMember Properties

        [MessageField(1, SerializeSize = ArraySizeType.X3F1)]
        public SpecialAttack[] Specials { get; set; }

        [MessageField(2)]
        public int Unknown1 { get; set; }

        [MessageField(3)]
        public int Unknown2 { get; set; }

        [MessageField(4)]
        public int Unknown3 { get; set; }

        [MessageField(5)]
        public int Unknown4 { get; set; }

        [MessageField(6)]
        public int AggDef { get; set; }

        #endregion
    }
}