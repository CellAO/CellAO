﻿#region License

// Copyright (c) 2005-2014, CellAO Team
// 
// 
// All rights reserved.
// 
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 
// 
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//     * Neither the name of the CellAO Team nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#endregion

namespace CellAO.Messages.N3Messages
{
    using CellAO.Enums;
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    #region Usings ...

    using CellAO.Messages.Fields;

    #endregion

    [AoContract((int)N3MessageType.ChestItemFullUpdate)]
    public class ChestItemFullUpdateMessage : N3Message
    {
        public ChestItemFullUpdateMessage() {
            this.N3MessageType = N3MessageType.ChestItemFullUpdate;
        }

        [MessageField(0)]
        public int SifuMsgVersion { get; set; }

        [MessageField(1)]
        public Identity Owner { get; set; }

        [MessageField(2)]
        public int Playfield { get; set; }

        [MessageField(3)]
        public Identity Unknown1 { get; set; }

        [MessageField(4)]
        public byte Unknown2 { get; set; }

        [MessageField(5)]
        public byte Unknown3 { get; set; }

        [MessageField(6, SerializeSize = ArraySizeType.X3F1)]
        public GameTuple<StatIds, uint>[] Stats { get; set; }

        [MessageField(7, SerializeSize = ArraySizeType.Int32)]
        public string Name { get; set; }

        [MessageField(8)]
        public int LifuMsgVersion { get; set; }

        [MessageField(9)]
        public int Unknown4 { get; set; }

        [MessageField(10)]
        public int Unknown5 { get; set; }

        [MessageField(11)]
        public int CifuMsgVersion { get; set; }
    }
}