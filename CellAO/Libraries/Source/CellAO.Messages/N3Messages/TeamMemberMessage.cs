﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamMemberMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the TeamMemberMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.TeamMember)]
    public class TeamMemberMessage : N3Message
  {
        #region Constructors and Destructors

        public TeamMemberMessage()
        {
            this.N3MessageType = N3MessageType.TeamMember;
            this.Unknown = 0x00;
            this.Unknown2 = 0xFFFFFFFF;
            this.Unknown3 = 0x00000001;
            this.Unknown4 = 0x0004;
    }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public Identity Character { get; set; }

        [MessageField(1)]
        public Identity Team { get; set; }

        [MessageField(2)]
        public uint Unknown2 { get; set; }

        [MessageField(3)]
        public int Unknown3 { get; set; }

        [MessageField(4)]
        public short Unknown4 { get; set; }

        [MessageField(5, SerializeSize = ArraySizeType.Int32)]
        public string Name { get; set; }

        #endregion
    }
}