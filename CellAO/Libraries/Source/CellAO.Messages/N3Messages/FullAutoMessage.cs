﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces.Attributes;

    [AoContract((int)N3MessageType.FullAuto)]
    public class FullAutoMessage:N3Message
    {
        public FullAutoMessage()
        {
            this.N3MessageType = N3MessageType.FullAuto;
        }

        [MessageField(1)]
        public int Unknown1 { get; set; }

        [MessageField(2)]
        public int Unknown2 { get; set; }

    }
}
