﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="N3TeleportMessage.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the N3TeleportMessage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.N3Messages
{
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;
    using CellAO.Messages.Fields;

    [AoContract((int)N3MessageType.N3Teleport)]
    public class N3TeleportMessage : N3Message
    {
        #region Constructors and Destructors

        public N3TeleportMessage()
        {
            this.N3MessageType = N3MessageType.N3Teleport;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public Vector3 Destination { get; set; }

        [MessageField(1)]
        public Quaternion Heading { get; set; }

        [MessageField(2)]
        public byte Unknown1 { get; set; }

        [MessageField(3)]
        public Identity Playfield { get; set; }

        [MessageField(4)]
        public int GameServerId { get; set; }

        [MessageField(5)]
        public int SgId { get; set; }

        [MessageField(6)]
        public Identity ChangePlayfield { get; set; }

        [MessageField(7)]
        public int Unknown4 { get; set; }

        [MessageField(8)]
        public int Unknown5 { get; set; }

        [MessageField(9)]
        public Identity Playfield2 { get; set; }

        [MessageField(10)]
        public int Unknown6 { get; set; }

        /* These are not in the packet from the client
        [AoMember(11)]
        public int Unknown7 { get; set; }

        [AoMember(12)]
        public int Unknown8 { get; set; }
        */
        #endregion
    }
}