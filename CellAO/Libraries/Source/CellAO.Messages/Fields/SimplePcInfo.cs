﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SimplePcInfo.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the SimplePcInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces.Attributes;

    public class SimplePcInfo : SimpleCharacterInfo
    {
        #region AoMember Properties

        [MessageField(0)]
        public uint CurrentNano { get; set; }

        [MessageField(1)]
        public int Team { get; set; }

        [MessageField(2)]
        public short Swim { get; set; }

        [MessageField(3)]
        public short StrengthBase { get; set; }

        [MessageField(4)]
        public short AgilityBase { get; set; }

        [MessageField(5)]
        public short StaminaBase { get; set; }

        [MessageField(6)]
        public short IntelligenceBase { get; set; }

        [MessageField(7)]
        public short SenseBase { get; set; }

        [MessageField(8)]
        public short PsychicBase { get; set; }

        [MessageField(9, SerializeSize = ArraySizeType.Int16)]
        public string FirstName { get; set; }

        [MessageField(10, SerializeSize = ArraySizeType.Int16)]
        public string LastName { get; set; }

        [MessageField(11, SerializeSize = ArraySizeType.Int16)]
        public string OrgName { get; set; }

        #endregion
    }
}