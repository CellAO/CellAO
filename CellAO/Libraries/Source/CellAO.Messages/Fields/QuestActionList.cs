﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;
    using CellAO.Interfaces;

    public class QuestActionList
    {
        // Maybe Version id again?
        [MessageField(0)]
        public int Version { get; set; }
        [MessageField(1)]
        public Identity Action { get; set; }

        [MessageField(2)]
        public Identity Unknown1 { get; set; }
        [MessageField(3)]
        public Identity Unknown2{ get; set; }
        [MessageField(4)]
        public Identity Unknown3 { get; set; }
        [MessageField(5)]
        public Identity Unknown4 { get; set; }

        [MessageField(6)]
        public float Unknown5 { get; set; }
        [MessageField(7)]
        public float Unknown6 { get; set; }
        [MessageField(8)]
        public float Unknown7 { get; set; }
        [MessageField(9)]
        public float Unknown8 { get; set; }
        [MessageField(10)]

        public Identity Unknown9 { get; set; }
        [MessageField(11)]
        public float Unknown10 { get; set; }
        [MessageField(12)]
        public float Unknown11 { get; set; }
        [MessageField(13)]
        public float Unknown12 { get; set; }
        [MessageField(14)]
        public float Unknown13 { get; set; }
        [MessageField(15)]
        public Identity Unknown14 { get; set; }

        [MessageField(16)]
        public int UnknownHash15 { get; set; }
        [MessageField(17)]
        public int Unknown16 { get; set; }
        [MessageField(18)]
        public Identity Unknown17 { get; set; }

        [MessageField(19)]
        public Identity Playfield { get; set; }

        // Probably low and high id of the entrance
        [MessageField(20)]
        public int Unknown18 { get; set; }
        [MessageField(21)]
        public int Unknown19 { get; set; }
        [MessageField(22)]
        public float X { get; set; }
        [MessageField(23)]
        public float Y { get; set; }
        [MessageField(24)]
        public float Z { get; set; }

    }
}
