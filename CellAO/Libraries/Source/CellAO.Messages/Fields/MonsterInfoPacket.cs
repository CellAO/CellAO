﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MonsterInfoPacket.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the MonsterInfoPacket type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class MonsterInfoPacket : InfoPacket
    {
        [MessageField(0)]
        public byte Unknown1 { get; set; }
        [MessageField (1)]
        public byte Profession { get; set; }
        [MessageField(2)]
        public byte Level { get; set; }
        [MessageField(3)]
        public byte TitleLevel { get; set; }
        [MessageField(4)]
        public byte VisualProfession { get; set; }
        [MessageField(5)]

        public short Unknown2 { get; set; }
        [MessageField(6)]
        public int CurrentHealth { get; set; }
        [MessageField(7)]
        public int MaxHealth { get; set; }
        [MessageField(8)]
        // Perhaps breed hostility or NPCFamily
        public int Unknown3 { get; set; }
        [MessageField(9)]
        public int OrganizationId { get; set; }
        [MessageField(10)]
        public short Unknown4 { get; set; }
        [MessageField(11)]
        public short Unknown5 { get; set; }
        [MessageField(12)]
        public short Unknown6 { get; set; }
        [MessageField(13)]
        public short Unknown7 { get; set; }
        [MessageField(14)]
        // 1234567890
        public int Unknown8 { get; set; }
        [MessageField(15)]
        // 1234567890
        public int Unknown9 { get; set; }
        [MessageField(16)]
        // 1234567890
        public int Unknown10 { get; set; }

    }
}