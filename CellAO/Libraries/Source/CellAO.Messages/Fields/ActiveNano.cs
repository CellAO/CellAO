﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveNano.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the ActiveNano type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class ActiveNano
    {
        #region AoMember Properties

        [MessageField(0)]
        public int Unknown1 { get; set; }

        [MessageField(1)]
        public int NanoId { get; set; }

        [MessageField(2)]
        public int NanoInstance { get; set; }

        [MessageField(3)]
        public int Time1 { get; set; }

        [MessageField(4)]
        public int Time2 { get; set; }


        #endregion
    }
}