﻿using CellAO.Interfaces;
using CellAO.Interfaces.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{

    public class FullCharacterSub2
    {
        [MessageField(1)]
        public int Unknown1 { get; set; }
        [MessageField(2)]
        public Identity Unknown2 { get; set; }
        [MessageField(3)]
        public int Unknown3 { get; set; }
        [MessageField(4)]
        public int Unknown4 { get; set; }
    }
}
