﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    public class QuestCharInfo
    {
        [MessageField(0)]
        public Identity CharacteIdentity { get; set; }
        [MessageField(1, SerializeSize = ArraySizeType.Int32)]
        public string CharacterName { get; set; }
    }
}
