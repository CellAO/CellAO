﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoginCharacterInfo.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the LoginCharacterInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Enums;
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces;
    using CellAO.Interfaces.Attributes;

    public class LoginCharacterInfo
    {
        #region Constructors and Destructors

        public LoginCharacterInfo()
        {
            this.Unknown1 = 4;
            this.Unknown2 = 1;
        }

        #endregion

        #region AoMember Properties

        [MessageField(0)]
        public int Unknown1 { get; set; }

        [MessageField(1)]
        public int Id { get; set; }

        [MessageField(2)]
        public byte PlayfieldProxyVersion { get; set; }

        [MessageField(3)]
        public Identity PlayfieldId { get; set; }

        [MessageField(4)]
        public int PlayfieldAttribute { get; set; }

        [MessageField(5)]
        public int ExitDoor { get; set; }

        [MessageField(6)]
        public Identity ExitDoorId { get; set; }

        [MessageField(7)]
        public int Unknown2 { get; set; }

        [MessageField(8)]
        public int CharacterInfoVersion { get; set; }

        [MessageField(9)]
        public int CharacterId { get; set; }

        [MessageField(10)]
        public int Unknown3 { get; set; }

        [MessageField(11, SerializeSize = ArraySizeType.Int32)]
        public string Name { get; set; }

        [MessageField(12)]
        public Breed Breed { get; set; }

        [MessageField(13)]
        public Gender Gender { get; set; }

        [MessageField(14)]
        public Profession Profession { get; set; }

        [MessageField(15)]
        public int Level { get; set; }

        [MessageField(16, SerializeSize = ArraySizeType.Int32)]
        public string AreaName { get; set; }

        [MessageField(17)]
        public int Unknown4 { get; set; }

        [MessageField(18)]
        public int Unknown5 { get; set; }

        [MessageField(19)]
        public int Unknown6 { get; set; }

        [MessageField(20)]
        public int Unknown7 { get; set; }

        [MessageField(21)]
        public int Unknown8 { get; set; }

        [MessageField(22)]
        public CharacterStatus Status { get; set; }

        #endregion
    }
}