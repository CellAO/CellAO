﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ActiveNano.cs" company="SmokeLounge">
//   Copyright © 2013 SmokeLounge.
//   This program is free software. It comes without any warranty, to
//   the extent permitted by applicable law. You can redistribute it
//   and/or modify it under the terms of the Do What The Fuck You Want
//   To Public License, Version 2, as published by Sam Hocevar. See
//   http://www.wtfpl.net/ for more details.
// </copyright>
// <summary>
//   Defines the ActiveNano type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CellAO.Messages.Fields
{
    using CellAO.Interfaces.Attributes;

    public class ItemAnimEffect
    {
        #region AoMember Properties

        [MessageField(0)]
        public int Unknown1 { get; set; }

        [MessageField(1)]
        public int NanoId { get; set; }

        [MessageField(2)]
        public int NanoInstance { get; set; }

        [MessageField(3)]
        public int Time1 { get; set; }

        [MessageField(4)]
        public int Time2 { get; set; }

        [MessageField(4)]
        public int Unknown2 { get; set; }
        [MessageField(5)]
        public int Unknown3 { get; set; }
        [MessageField(6)]
        public int Unknown4 { get; set; }
        [MessageField(7)]
        public int Unknown5 { get; set; }
        [MessageField(8)]
        public int Unknown6 { get; set; }
        [MessageField(9)]
        public int Unknown7 { get; set; }
        [MessageField(10)]
        public int Unknown8 { get; set; }
        [MessageField(11)]
        public int Unknown9 { get; set; }
        [MessageField(12)]
        public int Unknown10 { get; set; }
        [MessageField(13)]
        public int Unknown11 { get; set; }

        #endregion
    }
}