﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CellAO.Messages.Fields
{
    using CellAO.Enums.Serialization;
    using CellAO.Interfaces.Attributes;
    
    public class QuestInfo
    {
        // Unique identity for this quest
        [MessageField(0)]
        public CellAO.Interfaces.Identity QuestIdentity { get; set; }

        // Always 0x0f?
        [MessageField(1)]
        public int Unknown1 { get; set; }

        [MessageField(2)]
        public int Unknown2 { get; set; }

        [MessageField(3)]
        public int Unknown3 { get; set; }

        [MessageField(4)]
        public int Unknown4 { get; set; }

        [MessageField(5, SerializeSize = ArraySizeType.NoSerialization, FixedSizeLength = 32)]
        public string ShortInfo { get; set; }

        [MessageField(6, SerializeSize = ArraySizeType.Int32)]
        public string Info { get; set; }
        [MessageField(7)]
        public CellAO.Interfaces.Identity Unknown5 { get; set; }

        // We need to distinguish later for the older versions (actual version=6)
        [MessageField(8)]
        public int RewardDescriptorVersion { get; set; }

        [MessageField(9)]
        public int CashReward { get; set; }
        // a null again?
        [MessageField(10)]
        public int Unknown6 { get; set; }
        [MessageField(11)]
        public int ExperienceReward { get; set; }

        [MessageField(12, SerializeSize = ArraySizeType.X3F1)]
        public CellAO.Interfaces.Identity[] UnknownIdentities1 { get; set; }
        [MessageField(13, SerializeSize = ArraySizeType.X3F1)]
        public CellAO.Interfaces.Identity[] UnknownIdentities2 { get; set; }

        [MessageField(14, SerializeSize = ArraySizeType.X3F1)]
        public QuestItemShort[] ItemRewards { get; set; }

        [MessageField(15)]
        public int Unknown7 { get; set; }
        [MessageField(16)]
        public int Unknown8 { get; set; }
        [MessageField(17)]
        public int Unknown9 { get; set; }
        [MessageField(18)]
        public int UnknownHash { get; set; }
        [MessageField(19)]
        public int Quality { get; set; }

        [MessageField(20)]
        public int Unknown10 { get; set; }
        [MessageField(21)]
        public int Unknown11 { get; set; }
        [MessageField(22)]
        public int Unknown12 { get; set; }
        [MessageField(23)]

        public int Unknown13 { get; set; }

        [MessageField(24)]
        public CellAO.Interfaces.Identity Unknown14 { get; set; }
        [MessageField(25)]
        public int MissionIconId { get; set; }
        [MessageField(26)]
        public int Unknown15 { get; set; }
        [MessageField(27)]
        public int Unknown16 { get; set; }
        [MessageField(28, SerializeSize = ArraySizeType.X3F1)]
        public QuestActionList[] QuestActions { get; set; }
        [MessageField(29, SerializeSize = ArraySizeType.X3F1)]
        public CellAO.Interfaces.Identity[] Unknown17 { get; set; }

        [MessageField(30, SerializeSize = ArraySizeType.Int32)]
        public int[] Unknown18 { get; set; }
        [MessageField(31, SerializeSize = ArraySizeType.Int32)]
        public int[] Unknown19 { get; set; }

        [MessageField(32, SerializeSize = ArraySizeType.Int32)]
        public QuestCharInfo[] CharInfos { get; set; }

        [MessageField(33)]
        public int Unknown20 { get; set; }
        [MessageField(34, SerializeSize = ArraySizeType.X3F1)]
        public CellAO.Interfaces.Identity[] UnknownIdentities20 { get; set; }

        [MessageField(35)]
        public int Unknown21 { get; set; }
        [MessageField(36)]
        public int Unknown22 { get; set; }

        [MessageField(37)]
        public CellAO.Interfaces.Identity Unknown23 { get; set; }

        [MessageField(38)]
        public int Unknown24 { get; set; }
        [MessageField(39)]
        public int Unknown25 { get; set; }

        [MessageField(40, SerializeSize = ArraySizeType.Int32)]
        public QuestIdentity[] QuestIdentities { get; set; }
        [MessageField(41)]
        public int Unknown26 { get; set; }
        [MessageField(42, SerializeSize = ArraySizeType.X3F1)]
        public QuestFaction[] FactionInfo { get; set; }

        [MessageField(43)]
        public byte Unknown27 { get; set; }
    }
}
