﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CellAO.Core.Nanos
{
    using ZoneEngine.Core;
    using CellAO.Core.Entities;
    /// <summary>
    /// Represents an active Nano's function applied to a character
    /// </summary>
    public class ActiveNanoFunction : CellAO.Interfaces.IActiveNanoFunction
    {
        public ActiveNanoFunction(ICharacter caster, ICharacter target, NanoFormula nano, Functions.Function func, uint funcIndex, DateTime? lastTick = null, DateTime? firstCasted = null)
        {
            this.TickCounter = func.TickCount > 0 ? func.TickCount : 1;
            this.TickInterval = (uint)((uint)func.TickInterval > (uint)0 ? (uint)func.TickInterval : (uint) nano.getItemAttribute((int)CellAO.Enums.StatIds.timeexist));
            this.Nanotype = nano.NanoStrain();
            this.ID = nano.ID;
            this.Instance = new Random().Next(1,999999);
            this.Caster = caster;
            this.Host = target;
            this.Nano = nano;
            this.Function = func;
            this.FunctionIndex = funcIndex;
            this.LastTick = lastTick.HasValue ? lastTick.Value : DateTime.MinValue;
            this.FirstCasted = firstCasted.HasValue ? firstCasted.Value : DateTime.UtcNow;
        }
        public Functions.Function Function { get; set; }
        public uint FunctionIndex { get; set; }
        public ICharacter Caster { get; set; }
        public ICharacter Host { get; set; }
        public NanoFormula Nano { get; set; }
        public int ID { get; set; }
        public int Instance { get; set; }
        public int Nanotype { get; set; }
        public int CurrentTickCount { get; set; }
        public int TickCounter { get; set; }
        public uint TickInterval { get; set; }
        public DateTime LastTick { get; set; }
        public DateTime FirstCasted { get; set; }
        public bool Expired { get; set; }
        public int Value3 { get; set; }

        /// <summary>
        /// Call to "expire" a nano. This allows for certain functions to reverse their effects (Modify)
        /// </summary>
        public void Expire()
        {
            Host.Controller.ReverseFunction(Function, Host);
            Expired = true;
        }

        /// <summary>
        /// Executes one "tick" of a timed function like DoTs and HoTs
        /// </summary>
        public void Tick()
        {
            Host.Controller.CallFunction(Function, Caster);

            CurrentTickCount = CurrentTickCount + 1;
            LastTick = DateTime.UtcNow;
            if (CurrentTickCount > TickCounter)
            {
                Expire();
            }
        }
    }
}
