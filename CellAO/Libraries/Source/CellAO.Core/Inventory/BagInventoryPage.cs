﻿using CellAO.Enums;
using CellAO.Interfaces;

namespace CellAO.Core.Inventory
{
    public class BagInventoryPage : BaseInventoryPage
    {
        #region Constructors and Destructors

        public bool SentCifu { get; set; }

        public BagInventoryPage(Identity ownerInstance, int SlotNumberInMainInventory)
            : base((int)IdentityType.Bag, 21, 0, ownerInstance, SlotNumberInMainInventory) {
            this.SentCifu = false;
        }

        #endregion
    }
}