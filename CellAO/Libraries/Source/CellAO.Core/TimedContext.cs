﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace CellAO.Core
{
    public interface ITimedAction
    {
        bool Paused { get; set; }
        bool Processed { get; set; }
        int Id { get; set; }
        int AttackCycleRemaining { get; set; }
        int RechargeCycleRemaining { get; set; }
        void Execute();
        void FinishedRecharge();
    }
    public abstract class TimedContext
    {
        internal int TIMER_DELAY = 80;
        internal Timer QueueWorker { get; set; }
        public virtual Dictionary<int, List<ITimedAction>> Queue { get; set; }

        internal DateTime lastTicked = DateTime.MinValue;

        public TimedContext()
        {
            this.Queue = new Dictionary<int, List<ITimedAction>>();
            this.QueueWorker = new Timer();
            this.QueueWorker.Interval = TIMER_DELAY;
            this.QueueWorker.Elapsed += QueueWorker_Elapsed;
            this.QueueWorker.Enabled = true;
        }

        private void QueueWorker_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                int decreaseby = TIMER_DELAY;
                if (lastTicked != DateTime.MinValue)
                {
                    var datecache = DateTime.UtcNow;
                    decreaseby = (int)(datecache - lastTicked).TotalMilliseconds;
                    lastTicked = datecache;
                }
                else
                {
                    lastTicked = DateTime.UtcNow;
                }

                bool decreasedOne = false;
                var queueCopy = Queue.Select(x => x.Key).ToList();
                foreach (int key in queueCopy)
                {
                    //Utility.LogUtil.Debug(Utility.DebugInfoDetail.Error, "Working on " + key + " - " + (Queue[key].Count > 0 ? (Queue[key].FirstOrDefault() as CharacterAction).Character.Name : "") + " ActionCount: " + Queue[key].Count);
                    if (!Queue.ContainsKey(key) || Queue[key].Count == 0) continue;
                    

                    List<ITimedAction> listCopy = Queue[key].ToList();

                    //Utility.LogUtil.Debug(Utility.DebugInfoDetail.Error, String.Format("Actions piled up: {0}   {1}  {2}" , Queue[key].Count, key, String.Join(", ", Queue[key].Select(a=>((CharacterAction)a).Type.ToString()))));
                    foreach (ITimedAction action in listCopy.Where(x => !x.Processed))
                    {
                        if (action.Paused)
                        {
                            break;
                        }
                        action.AttackCycleRemaining -= decreaseby;
                        //Utility.LogUtil.Debug(Utility.DebugInfoDetail.Error, "Decrease charge " + action.GetHashCode() + " " + action.AttackCycleRemaining + " source: " + (action as CharacterAction).Character.Name);
                        decreasedOne = true;
                        if (action.AttackCycleRemaining <= 0)
                        {
                            //Utility.LogUtil.Debug(Utility.DebugInfoDetail.Error, "Execute Fightactiontype: " + ((CharacterAction)action).Type.ToString());
                            
                            try
                            {
                                action.Execute();
                            }
                            catch (Exception ex)
                            {
                                Utility.LogUtil.WarnException(ex);
                            }
                            //Utility.LogUtil.Debug(Utility.DebugInfoDetail.Error, "MARK PROCESSED Fightactiontype: " + fa.Type.ToString());
                            action.Processed = true;
                        }
                        break;
                    }
                    if (decreasedOne)
                    {
                        continue;
                    }
                    //Utility.LogUtil.Debug(Utility.DebugInfoDetail.Error, "Recharging stage");
                    foreach (ITimedAction action in listCopy.Where(x => x.Processed))
                    {
                        {
                            action.RechargeCycleRemaining -= decreaseby;
                            //Utility.LogUtil.Debug(Utility.DebugInfoDetail.Error, "Decrease recharge " + action.GetHashCode()+" "+ action.RechargeCycleRemaining + " source: " + (action as CharacterAction).Character.Name);
                            if (action.RechargeCycleRemaining <= 0)
                            {
                                //Utility.LogUtil.Debug(Utility.DebugInfoDetail.Error, "Removed from queue " + (action as FightAction).Type.ToString());
                                action.FinishedRecharge();
                                Queue[key].Remove(action);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.LogUtil.WarnException(ex);
            }
            finally
            {
                //System.Threading.Monitor.Exit(sender);
            }

        }

        /// <summary>
        /// Cancels any actions that are waiting to fire and/or are in the attack phase
        /// </summary>
        /// <param name="id"></param>
        public void WipeUnprocessedActions(int queueId)
        {
            if (Queue.ContainsKey(queueId))
            {
                Queue[queueId].RemoveAll(a => !a.Processed);
            }
        }

        public void PauseQueue(int id)
        {
            if (Queue.ContainsKey(id))
            {
                foreach (var action in Queue[id])
                {
                    action.Paused = true;
                }
            }
        }

        public void Push(ITimedAction action)
        {
            if (!Queue.ContainsKey(action.Id))
                Queue.Add(action.Id, new List<ITimedAction>());
            Queue[action.Id].Add(action);
        }

        public void PushToFront(ITimedAction action)
        {
            if (!Queue.ContainsKey(action.Id))
                Queue.Add(action.Id, new List<ITimedAction>());
            if (Queue[action.Id].Count > 0)
            {
                Queue[action.Id].Insert(0, action);
            }
            else
            {
                Queue[action.Id].Add(action);
            }
        }
        public void CancelQueue(int id)
        {
            Queue.Remove(id);
        }
        public void Pluck(int queueId, Func<ITimedAction, bool> predicate)
        {
            if (Queue.ContainsKey(queueId))
                Queue[queueId] = Queue[queueId].Where(predicate).ToList();
        }
        public void ResumeQueue(int id)
        {
            if (Queue.ContainsKey(id))
            {
                foreach (var action in Queue[id])
                {
                    action.Paused = false;
                }
            }
        }
    }
}
